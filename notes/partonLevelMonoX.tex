\documentclass[12pt,a4paper]{article}

\usepackage{amsmath}
\usepackage{fullpage}
\usepackage{hepunits}
\usepackage{color}
\usepackage{tikz}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{decorations.pathreplacing}

\usepackage{graphicx}
\graphicspath{{../figures/}}

\newcommand{\abs}[1]{| #1 |}
\newcommand{\dif}{\mathrm{d}}

\newcommand{\dc}[1]{\textcolor{blue}{DS: #1}}
\begin{document}

\section{Definitions}

Consider the following parton-level event

\begin{tikzpicture}[scale=2]
\draw (-1,0) node[left] {$p_1$} -- (0,0);
\draw (1,0) node[right] {$p_2$} -- (0,0);
\draw (0,0) -- (35:1) node[right] {$p_3$};
\draw[dashed] (0,0) -- (250:1);
\draw[dashed] (0,0) -- (260:1);
\draw[dashed] (0,0) -- (240:1);
\draw [decorate,decoration={brace},yshift=-4pt,xshift=-2pt] (265:1) -- (235:1) node [black,midway,yshift=-8pt,xshift=-4pt] {$p_4$};
\end{tikzpicture}

\noindent
where particle 3 is a massless and sole visible final state object and w.l.o.g.~the four momentum $p_4$ is the sum of the momenta of all the unseen stuff in the final state, with invariant mass $p_4^2 = M^2$. Parameterise the four momenta as
\begin{align*}
p_1^\mu &= x_1 E_b (1,0,0,1) \\ 
p_2^\mu &= x_2 E_b (1,0,0,-1) \\ 
p_3^\mu &= (p_T \cosh y,\vec{p_T}, p_T \sinh y) \\ 
p_4^\mu &= (\sqrt{p_T^2 + M^2} \cosh y_i,-\vec{p_T}, \sqrt{p_T^2 + M^2} \sinh y_i) 
\end{align*}
where the two component vector $p_T$ stands in for the $x$ and $y$ components of the four vectors, and $p_T = \abs{\vec{p_T}}$. Hereafter, let's work in units where the beam energy $E_b = \frac{1}{2}$, such that $s=1$.

One of the advantages of working in these parameters is that, under a longitudinal boost
\begin{equation}
p_3^\mu \to
\begin{pmatrix}
\cosh \eta & 0 & 0 & \sinh \eta \\ 
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
\sinh \eta & 0 & 0 & \cosh \eta  
\end{pmatrix} p_3
\end{equation}
they behave as
\begin{align}
p_T &\to p_T, \\
y &\to y + \eta \, .
\end{align}

\section{Distributions}

We can make two (physically significant) measurements of the visible final state in each event: $p_T$ and $y$ (or any equivalent basis, such as energy and angle with the beam axis). If we knew $x_1$ and $x_2$ (and $M$), we could plot the possible values of $p_T$ and $y$ as a 1d curve satisfying
\begin{equation}
p_T \cosh \left( y + \eta \right) = \frac{\sqrt{x_1 x_2}}{2} \left( 1 - \frac{M^2}{x_1 x_2} \right)
\label{eq:ptyrel}
\end{equation}
where
\begin{equation}
\tanh \eta = \frac{x_2-x_1}{x_1 + x_2} = \mathrm{sgn}(x_2 - x_1) \sqrt{\frac{(x_2 -x_1)^2}{(x_2-x_1)^2 + 4 x_1 x_2}}
\end{equation}
To prove this, expand the RHS of $p_4^2 = M^2 = (p_1 +p_2 - p_3)^2$:
\begin{align*}
M^2 &= (p_1+p_2)^2 - 2 (p_1 + p_2)\cdot p_3 \\
&= x_1 x_2 -p_T \left( (x_1 +x_2) \cosh y + (x_2 - x_1) \sinh y \right) \\
&= x_1 x_2 - A p_T \cosh (y + \eta)
\end{align*}
To calculate $A$ and $\eta$, observe that
\begin{align*}
A \cosh (y+\eta) = A \cosh y \cosh \eta + A \sinh y \sinh \eta \\
\implies A \cosh \eta = x_1 + x_2 ; \quad A \sinh \eta = x_2 - x_1 \\
\implies A^2 = (x_1 +x_2)^2 - (x_2 - x_1)^2 = 4 x_1 x_2
\end{align*}

One can then understand the effects of changing $x_1$ and $x_2$ on the distribution of $p_T$ and $y$ values, or at least, to first order, the effects of changing the two combinations of $x_1 x_2$ and $x_1 - x_2$. The former rescales the curve along the $p_T$ axis; the latter translates along the $y$ axis. See Fig. \ref{fig:ptycurves}.

\begin{figure}
\includegraphics[width=12cm]{partonLevelCurves.pdf}
\caption{\label{fig:ptycurves}}
\end{figure}

We can then compute the partonic cross section (still in $E_b = \frac{1}{2}$ units) differential in $y$, assuming the matrix element squared is azimuthally symmetric 
\begin{align*}
\hat \sigma_{2 \to 2} &= \frac{1}{4 E_1 E_2 \abs{v_1-v_2}} \int \frac{\dif^3 p_3}{(2\pi)^3} \frac{\dif^3 p_4}{(2\pi)^3} \frac{1}{2 E_3 2 E_4} (2 \pi)^4 \delta^{(4)}(\sum p_i) \abs{\mathcal{M}}^2 \\
 &= \frac{1}{32 \pi^2 x_1 x_2} \int \dif^3 p_3 \dif^3 p_4 \frac{1}{E_3 E_4} \delta^{(4)}(\sum p_i) \abs{\mathcal{M}}^2 \\
 &= \frac{1}{16 \pi x_1 x_2} \int \dif p_T \dif y  \, p_T^2 \cosh y \frac{1}{E_3 E_4} \delta(\frac{1}{2}(x_1 + x_2) - E_3 - E_4) \abs{\mathcal{M}}^2 
\end{align*}
To integrate over the delta function in energy, we need to know the dependence of $E_3$ and $E_4$ on $p_T$ and $y$, without assuming the energy conservation implied by the delta function, then calculate the partial derivative of the delta function's argument w.r.t $p_T$, and then we can impose energy convservation. We have
\begin{align*}
E_3 &= p_T \cosh y \\
E_4^2 &= \vec{p_4}^2 + M^2 \\
&= p_T^2 + \left( \frac12 (x_1 -x_2) - p_T \sinh y \right)^2 + M^2 \\
&= p_T^2 \cosh^2 y + \frac14 (x_1 -x_2)^2 + M^2 - p_T (x_1 - x_2) \sinh y \\
\implies & \frac{ \partial E_3}{\partial p_T} \Big|_y = \cosh y \\
\implies & 2 E_4 \frac{ \partial E_4}{\partial p_T} \Big|_y = 2 p_T \cosh^2 y - (x_1 -x_2) \sinh y \\
\implies & E_3 E_4 \left( \frac{ \partial E_3}{\partial p_T} \Big|_y + \frac{ \partial E_4}{\partial p_T} \Big|_y \right) = \sqrt{x_1 x_2} \cosh y \cosh (y + \eta)
\end{align*}
We now integrate w.r.t $p_T$:
\begin{align*}
 \hat \sigma_{2 \to 2} &= \frac{1}{16 \pi x_1 x_2} \int \dif y  \, p_T^2 \cosh y \left[ E_3 E_4 \left( \frac{ \partial E_3}{\partial p_T} \Big|_y + \frac{ \partial E_4}{\partial p_T} \Big|_y \right) \right]^{-1} \theta(x_1 x_2 - M^2) \abs{\mathcal{M}}^2 \\
 &= \frac{1}{32 \pi x_1^2 x_2^2} (x_1 x_2 -M^2) \theta(x_1 x_2 - M^2) \int \dif y  \,  \frac{1}{\cosh^2(y+\eta)} \abs{\mathcal{M}}^2 \\
\end{align*}
where in the last line we have now assumed energy conservation, i.e., assumed that the relation (\ref{eq:ptyrel}) holds. The $\cosh^{-2}(y+\eta)$ factor acts to keep the events close-ish to $y=\eta$ (it goes as $e^{-2 \abs{y}}$ as $y \to \pm \infty$). The full cross section is then
\begin{equation}
\sigma = \int \dif x_1 \dif x_2 f_1(x_1) f_2(x_2) \hat \sigma_{2 \to 2}
\end{equation}
for some p.d.fs. Defining variables $u=x_1 x_2$, $v = \frac{x_1 - x_2}{x_1 + x_2}$, we can write
\begin{equation}
\sigma = \frac{1}{32 \pi} \int_{M^2}^1 \dif u \int \dif v \int \dif y f_1 f_2 \frac{u-M^2}{u^2} \frac{1}{1-v^2} \frac{1}{\cosh^2(y - \tanh^{-1} v)} .
\end{equation}
Note that the inverted definitions are
\begin{equation}
x_1^2 = u \frac{1+v}{1-v} ; \quad
x_2^2 = u \frac{1-v}{1+v}
\end{equation}
whence the limits $0 \leq x_1,x_2 \leq 1$ imply
\begin{equation}
0 \leq u \leq 1; \quad \abs{v} \leq \frac{1-u}{1+u}
\end{equation}
Fig.\ref{fig:pdfsUV} plots the product of p.d.f.s $f_1 f_2$ in the space $u,v$, assuming up quark pdfs.

\begin{figure}
\includegraphics[width=5cm]{PDFsUV.pdf}
\includegraphics[width=5cm]{PDFsUarctanhV.pdf}
\caption{$f_1f_2$.\label{fig:pdfsUV}}
\end{figure}

In Figure \ref{fig:ptydist} assume these are up quark pdfs, and plot the distribution of events for $s=13 \TeV$ and a selection of $M$s when the matrix element squared is trivial. In Figure \ref{fig:ptycomp} we compare the distributions for different $M$. Whilst 1D distributions $p_T$ and $E$ help to some degree to discriminate between masses of the invisibile system, the 1D distribution in $y$ appears hopeless. However, in comparing the 2D distributions in $p_T$ and $y$, the $y$ information does appear to be useful in distinguishing the two.

\begin{figure}
\includegraphics[width=5cm]{PtYDistM200.pdf}
\includegraphics[width=5cm]{PtYDistM500.pdf}
\includegraphics[width=5cm]{PtYDistM2000.pdf}
\caption{The probability distribution of events in $p_T$ and $\abs{y}$ for up-quark-up-quark-production of a massless visible particle and heavy invisible particle of mass $M=200,500,2000 \GeV$ respectively, assuming trivial matrix element $\abs{\mathcal{M}}^2=1$.\label{fig:ptydist}}
\end{figure}

\begin{figure}
\includegraphics[width=5cm]{ptDist.pdf}
\includegraphics[width=5cm]{yDist.pdf}
\includegraphics[width=5cm]{EDist.pdf}
\includegraphics[width=5cm]{diffptYM200M2000.pdf}
\caption{The probability distributions of events for up-quark-up-quark-production of a massless visible particle and heavy invisible particle of mass $M=200,500,2000 \GeV$ respectively, assuming trivial matrix element $\abs{\mathcal{M}}^2=1$. In the last plot, the difference in bin counts for the $M=200 \GeV$ and $M=2000 \GeV$ cases when we generate 10000 events for each.\label{fig:ptycomp}}
\end{figure}


\dc{TODO: compute the above for ISR like matrix element squared!}

\end{document}
