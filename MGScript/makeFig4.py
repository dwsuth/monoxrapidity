#!/usr/bin/env python

import os
#import lhereader
import re
import math
import collections
#import hepmcio

from array import array
import numpy as np

import ROOT
from ROOT import TH1F

from LHCOTools import *

fig4block1=np.array([12.57012177190215,
6.662839069977674,
3.9129791949992634,
2.3572541742020854,
1.1141647740493739,
0.4251309801337883,
0.20086436450478787,
0.10243953269878416,
0.0375098814166879,
0.017869525529223346])

fig4block2=np.array([7.3050517536931014,
2.656443228016665,
1.3072176148484307,
0.7485885295890271,
0.30070859015933926,
0.08261718212126022,
0.042646512751529,
0.022864339046333233,
0.005057557849374628,
0.002836246517746987])

fig4block3=np.array([4.58169857737925,
0.853634065514012,
0.38950150181974974,
0.16898842009398155,
0.020750886943415786,
0.0048398539593067225,
0.0,
0.0,
0.0,
0.0])

ZZfig4heights=fig4block1-fig4block2
WZfig4heights=fig4block2-fig4block3

zhinvfig4heights=[
0.4795166518476471,
0.5421695683850917,
0.45330408700807845,
0.30195164029153265,
0.1560762664412512,
0.06263015704884382,
0.025105802078270362,
0.010598010163001433,
0.00408200430255612,
0.0026615437757708097]

DMsimpfig4heights=[
1.445731751817266,
1.2742749857031372,
1.002527954257903,
0.6778605362964779,
0.3989120846025514,
0.20691380811147986,
0.10076030505869185,
0.048451476651465084,
0.025774141622340308,
0.013538761800225556]

def makeFig4():
  ##define intrinsic properties of fig 4
  intluminosity=35900.
  xbinedges=[100.,125.,150.,175.,200.,250.,300.,350.,400.,500.,600.]
  ##use sampleDicts - a dictionary of dictionaries - to store the relevant variables for each sample of interest
  sampleDicts={
    'ZZ':{'reweightFunc':ZZreweight,'filename':'ZZevents.lhco'},
    'WZ':{'reweightFunc':WZreweight,'filename':'WZevents.lhco'},
    'DMsimpspin1':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1DECAY/med500dm150MODDELPH/med500dm150MODDELPHevents.lhco'}
  }
  
  ##fill individual histograms
  for lbl in sampleDicts:
    reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
    newhist=TH1F(lbl,lbl,len(xbinedges)-1,array('d',xbinedges))
    eventCount=0
    passCount=0
    for evt in reader:
      if all(cutfunc(evt) for cutfunc in cutFuncs):
        newhist.Fill(missPt(evt),evt.weight)
        passCount+=1
      eventCount+=1
    print '{0} of {1} events of {2} sample pass the cuts, SR xsec {3} pb, SR events {4}'.format(passCount,eventCount,lbl,reader.xsec*passCount/eventCount,intluminosity*reader.xsec*passCount/eventCount)
    newhist.Scale(intluminosity*reader.xsec/eventCount,"width")
    newhist.SetXTitle('ptmiss [GeV]')
    newhist.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['hist']=newhist
    sampleDicts[lbl]['xsec']=reader.xsec
    sampleDicts[lbl]['eventCount']=eventCount
    sampleDicts[lbl]['passCount']=passCount

  ## draw individual histograms
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['hist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['hist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4.pdf')

  ##k-factor adj
  print ''
  print 'After k-factors:'
  print 'The following results after k-factors are applied should have the same number of each type of events passing the cuts - as we have not changed any kinematic variables just rescaled their weights, the histograms however change as different k-factors are applied in different regions, meanwhile the SR region events and xsec are then scaled according to the new total passed cuts event weights over the previous before k factor adjustments'

  for lbl in sampleDicts:
    eventadjCount=0
    passadjCount=0
    totweight=0.
    totadjweight=0.
    reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
    newhistadj=TH1F(lbl+'_adj',lbl+'_adj',len(xbinedges)-1,array('d',xbinedges))
    for evt in reader:
      if all(cutfunc(evt) for cutfunc in cutFuncs):
        passadjCount+=1
        evtadjweight=sampleDicts[lbl]['reweightFunc'](evt)
        totweight+=evt.weight
        totadjweight+=evtadjweight
        newhistadj.Fill(missPt(evt),evtadjweight)  
      eventadjCount+=1
    sampleDicts[lbl]['totweight']=totweight
    sampleDicts[lbl]['totadjweight']=totadjweight
    newhistadj.Scale(intluminosity*reader.xsec/eventadjCount,"width")
    newhistadj.SetXTitle('ptmiss [GeV]')
    newhistadj.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['adjhist']=newhistadj
    print '{0} of {1} events of {4} sample pass the cuts, SR xsec {2} pb, SR events {3}'.format(sampleDicts[lbl]['passCount'],sampleDicts[lbl]['eventCount'],reader.xsec*totadjweight/totweight*sampleDicts[lbl]['passCount']/sampleDicts[lbl]['eventCount'],intluminosity*reader.xsec*totadjweight/totweight*sampleDicts[lbl]['passCount']/sampleDicts[lbl]['eventCount'],lbl)

  ##draw adjusted histgrams
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['adjhist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj.pdf')

  ##make stacked histogram
  fig4stack=ROOT.THStack('fig4','fig4')
  for lbl in sampleDicts: fig4stack.Add(sampleDicts[lbl]['hist'])
  fig4stack.Draw("hist")
  ROOT.gPad.SaveAs('fig4.pdf')

  ##make k-factor adjusted stacked histogram
  fig4stackadj=ROOT.THStack('fig4adj','fig4adj')
  for lbl in sampleDicts: fig4stackadj.Add(sampleDicts[lbl]['adjhist'])
  fig4stackadj.Draw("hist")
  ROOT.gPad.SaveAs('fig4kfactoradj.pdf')
  
  ####plot ratios
  wzhist=sampleDicts['WZ']['adjhist']
  zzhist=sampleDicts['ZZ']['adjhist']
  dmsimphist=sampleDicts['DMsimpspin1']['adjhist']
  for i in range(len(WZfig4heights)):
    wzhist.SetBinContent(i+1,wzhist.GetBinContent(i+1)/WZfig4heights[i])
    wzhist.SetBinError(i+1,wzhist.GetBinError(i+1)/WZfig4heights[i])
  wzhist.Draw()
  ROOT.gPad.SaveAs('ratioswzfig4.pdf')
  for i in range(len(ZZfig4heights)):
    zzhist.SetBinContent(i+1,zzhist.GetBinContent(i+1)/ZZfig4heights[i])
    zzhist.SetBinError(i+1,zzhist.GetBinError(i+1)/ZZfig4heights[i])
  zzhist.Draw()
  ROOT.gPad.SaveAs('ratioszzfig4.pdf')
  for i in range(len(DMsimpfig4heights)):
    dmsimphist.SetBinContent(i+1,dmsimphist.GetBinContent(i+1)/DMsimpfig4heights[i])
    dmsimphist.SetBinError(i+1,dmsimphist.GetBinError(i+1)/DMsimpfig4heights[i])
  dmsimphist.Draw()
  ROOT.gPad.SaveAs('ratiosdmsimpfig4.pdf')
####k-factor adjusted fig4
#  wzhistadj=next(h for h in hists if h.GetName()=='WZ')
#  zzhistadj=next(h for h in hists if h.GetName()=='ZZ')
#  DMsimphistadj=next(h for h in hists if h.GetName()=='DMsimpspin1')
#  for i in range(len(WZfig4heights)):
#    wzhistadj.SetBinContent(i, wzhistadj.GetBinContent(i)*1.109*1.03)
#  wzhistadj.Fill() 
#  wzhistadj.Draw()
#  ROOT.gPad.SaveAs('wzkfactoradjfig4.pdf')
#  for i in range(len(ZZfig4heights)):
#    if zzhistlog.GetBinContent(i)<1e-10: zzhistlog.SetBinContent(i,1e-10)
#    zzhistlog.SetBinContent(i, math.log10(zzhistlog.GetBinContent(i)))
#  zzhistlog.Draw()
#  ROOT.gPad.SaveAs('logzzfig4.pdf')
#  for i in range(len(DMsimpfig4heights)):
#    if DMsimphistlog.GetBinContent(i)<1e-10: DMsimphistlog.SetBinContent(i,1e-10)
#    DMsimphistlog.SetBinContent(i, math.log10(DMsimphistlog.GetBinContent(i)))
#  DMsimphistlog.Draw()
#  ROOT.gPad.SaveAs('logDMsimpfig4.pdf')
#  logfig4hist=next(h for h in hists)
#  for i in range(len(WZfig4heights)):
#     logfig4hist.SetBinContent(i,wzhistlog.GetBinContent(i)+zzhistlog.GetBinContent(i)+DMsimphistlog.GetBinContent(i))
#  logfig4hist.Draw()
#  ROOT.gPad.SaveAs('logfig4hist.pdf')

  
  return sampleDicts

def makeFig4FromSlimmed():
  ##define intrinsic properties of fig 4
  intluminosity=35900.
  xbinedges=[100.,125.,150.,175.,200.,250.,300.,350.,400.,500.,600.]
  ##use sampleDicts - a dictionary of dictionaries - to store the relevant variables for each sample of interest
  sampleDicts={
    'ZZ':{'reweightFunc':ZZreweight,'filename':'ZZevents.lhco','slimfile':'ZZ_slimmed.npz'},
    'WZ':{'reweightFunc':WZreweight,'filename':'WZevents.lhco','slimfile':'WZ_slimmed.npz'},
    #'DMsimpspin1':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1DECAY/med500dm150MODDELPH/med500dm150MODDELPHevents.lhco','slimfile':'DM_slimmed.npz'}
  }
  
  ##fill individual histograms
  for lbl in sampleDicts:
    sample=np.load(sampleDicts[lbl]['slimfile'])
    sampleDicts[lbl]['pts']=sample['pts']
    sampleDicts[lbl]['ys']=sample['ys']
    sampleDicts[lbl]['weights']=sample['weights']
    sampleDicts[lbl]['xsec']=sample['xsec'][0]
    sampleDicts[lbl]['eventCount']=len(sample['weights'])
    newhist=TH1F(lbl,lbl,len(xbinedges)-1,array('d',xbinedges))
    for misspt,wgt in np.nditer([sample['pts'],sample['weights']]):
      newhist.Fill(misspt,wgt)
    newhist.Scale(intluminosity*sampleDicts[lbl]['xsec']/sampleDicts[lbl]['eventCount'],"width")
    newhist.SetXTitle('ptmiss [GeV]')
    newhist.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['adjhist']=newhist

  ##draw adjusted histgrams
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['adjhist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj.pdf')

  ##make k-factor adjusted stacked histogram
  fig4stackadj=ROOT.THStack('fig4adj','fig4adj')
  for lbl in sampleDicts: fig4stackadj.Add(sampleDicts[lbl]['adjhist'])
  fig4stackadj.Draw("hist")
  ROOT.gPad.SaveAs('fig4kfactoradj.pdf')
  
  ####plot ratios
  wzhist=sampleDicts['WZ']['adjhist']
  zzhist=sampleDicts['ZZ']['adjhist']
  for i in range(len(WZfig4heights)):
    wzhist.SetBinContent(i+1,wzhist.GetBinContent(i+1)/WZfig4heights[i])
    wzhist.SetBinError(i+1,wzhist.GetBinError(i+1)/WZfig4heights[i])
  wzhist.Draw()
  ROOT.gPad.SaveAs('ratioswzfig4.pdf')
  for i in range(len(ZZfig4heights)):
    zzhist.SetBinContent(i+1,zzhist.GetBinContent(i+1)/ZZfig4heights[i])
    zzhist.SetBinError(i+1,zzhist.GetBinError(i+1)/ZZfig4heights[i])
  zzhist.Draw()
  ROOT.gPad.SaveAs('ratioszzfig4.pdf')
  dmsimphist=sampleDicts['DMsimpspin1']['adjhist']
  for i in range(len(DMsimpfig4heights)):
    dmsimphist.SetBinContent(i+1,dmsimphist.GetBinContent(i+1)/DMsimpfig4heights[i])
    dmsimphist.SetBinError(i+1,dmsimphist.GetBinError(i+1)/DMsimpfig4heights[i])
  dmsimphist.Draw()
  ROOT.gPad.SaveAs('ratiosdmsimpfig4.pdf')
  
  return sampleDicts


if __name__=='__main__':
  #sampleDicts=makeFig4()
  sampleDicts=makeFig4FromSlimmed()
