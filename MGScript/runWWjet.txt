launch WWjet
shower=PYTHIA8
madspin=OFF
madanalysis=ON
set nevents 100
set ptl 0.0
set ptj 7.0
set mll_sf 0.0
set jetalgo 1.0
set jetradius 1.0
set ickkw 3
set Qcut 30.0
set njmax 1
#edit shower --line_position=66 DM_3 = 23:onIfAll = 13 -13
#edit shower --line_position=66 DM_2 = 23:onIfAll = 11 -11
#edit shower --line_position=66 DM_1 = 23:onMode = 0
