#!/usr/bin/env python

import os,sys

MGdir=os.path.realpath(sys.argv[1])

for dirpath,subdirs,files in os.walk(MGdir):
  for fname in files:
    absfilepath=os.path.join(dirpath,fname)
    if os.path.islink(absfilepath):
      targetpath=os.path.realpath(absfilepath)
      #print(absfilepath,'\n    ',targetpath)
      if not targetpath.startswith(MGdir):
        print 'OUTSIDE '+targetpath
        tmppath=absfilepath+'.tmp'
        os.symlink(targetpath,tmppath)
        os.rename(tmppath,absfilepath)
      else: print 'INSIDE '+targetpath

