launch -f DMsimpspin1UONLYDECAY
shower=PYTHIA8
madspin=OFF
#set gVd31 0.001
set my1 800
set mxd 150
set nevents 100000
set ptl 0.0
set ptj 0.0
set mll_sf 0.0
set jetalgo -1
set jetradius 0.4
#edit madspin --replace_line="decay z" decay z > l+ l-
#edit madspin --after_line="decay z" decay y1 > xd xd~
#edit shower --replace_line="EXTRALIBS" EXTRALIBS    = stdhep Fmcfio dl  # Extra-libraries (not LHAPDF)
####new stuff 2019-02-19
#edit shower --after_line="Decay channels" DM_3 = 23:onIfAny = 13 -13
#edit shower --after_line="Decay channels" DM_2 = 23:onIfAny = 11 -11
#edit shower --after_line="Decay channels" DM_1 = 23:onMode = off
