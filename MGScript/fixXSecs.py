import os
import re
import sys

targetDirectory=os.path.abspath(sys.argv[1])

def insertXSec(LHCOfilename,xsec):
  with file(os.path.join(targetDirectory,LHCOfilename), 'r') as original: data = original.read()
  with file(os.path.join(targetDirectory,LHCOfilename), 'w') as modified: modified.write("##  Integrated weight (pb)  : "+xsec+'\n' + data)
  return

##get lhco file
LHCOfilename=next(fname for fname in os.listdir(targetDirectory) if '.lhco' in fname)

##check if xsec there
with open(os.path.join(targetDirectory,LHCOfilename),'r') as f:
  lhcotext=f.read()
stuff=re.search(r'Integrated weight \(pb\)  : (\S*)',lhcotext)
if stuff is not None:
  print 'LHCO file has xsec'
  print stuff.group(0)
  sys.exit(0)

##if no xsec, then try madspin
madspinfilename=next((fname for fname in os.listdir(targetDirectory) if 'decayed' in fname and '.txt' in fname),None)
if madspinfilename is not None:
  with open(os.path.join(targetDirectory,madspinfilename),'r') as f:
    mslines=f.readlines()
  xsec=mslines[-3].strip().split(' ')[0]
  print xsec
  insertXSec(LHCOfilename,xsec)
  sys.exit(0)
 
##if still no xsec, use summary
with open(os.path.join(targetDirectory,'summary.txt'),'r') as f:
  sumtext=f.read()
print sumtext
stuff=re.search(r'Total cross section: (\S*)',sumtext)
if stuff is not None:
  print 'found sum xsec'
  xsec=stuff.group(1)
  print xsec
  insertXSec(LHCOfilename,xsec)
  sys.exit(0)

