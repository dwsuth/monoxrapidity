#!/usr/bin/env python

import os
#import lhereader
import re
import math
import collections
#import hepmcio
from LHCO_reader import LHCO_reader


from array import array
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import ROOT
from ROOT import TH1F
from ROOT import TH2F


class Event:
  def __init__(self,weight,particles):
    self.weight=weight
    self.adjweight = weight
    self.particles=particles
#    self.xsec = xsec

class Particle:
  def __init__(self,pid,mom,charge):
    self.id=pid
    self.p=mom
    if charge is None:
      if self.id in [11,13,15]: self.charge=-1
      elif self.id in [-11,-13,-15]: self.charge=1
      else: self.charge=0
    else: self.charge=charge
    return

class EventIterator:
  def __init__(self,eventfile):
    self.reader=hepmcio.HepMCReader(os.path.join(targetdirectory,'Events',eventfile))
  def __iter__(self): return self
  def next(self):
    hepmcevt=self.reader.next()
    if not hepmcevt:
      raise StopIteration
    particles=[Particle(part.pid,ROOT.TLorentzVector(*tuple(part.mom)),None) for part in hepmcevt.particles.values()]
    if len(hepmcevt.weights)!=1: raise Exception
    return Event(hepmcevt.weights[0],particles)
    
class LHCOEventIterator:
  def getXSec(self,eventfile):
    with open(eventfile,'r') as f:
      text=f.read()
    stuff=re.search(r'Integrated weight \(pb\)  : (\S*)',text)
    return float(stuff.group(1))
  def __init__(self,eventfiles,maxEvents=None):
    if not isinstance(eventfiles,basestring):
      self._eventfilenames=eventfiles
    else: self._eventfilenames=[eventfiles]
    self._filecounter = 0
    self._eventcounter = 0
    self._totaleventcounter = 0
    self._maxEvents=maxEvents
    ##preload first event file
    print 'Loading ',self._eventfilenames[self._filecounter]
    self._events = LHCO_reader.Events(f_name=self._eventfilenames[self._filecounter],n_events=self._maxEvents)
    ##get cross section from first event file
    self.xsec=self.getXSec(self._eventfilenames[0])
  def __iter__(self): return self
  def next(self):
    if self._eventcounter>=len(self._events):
      if self._maxEvents is not None and self._eventcounter>=self._maxEvents: raise StopIteration
      if self._filecounter+1>=len(self._eventfilenames): raise StopIteration
      else:
        self._filecounter+=1
        self._eventcounter=0
        print 'Loading ',self._eventfilenames[self._filecounter]
        self._events = LHCO_reader.Events(f_name=self._eventfilenames[self._filecounter],n_events=(self._maxEvents-self._totaleventcounter if self._maxEvents else None))
    particles=[]
    for k,v in self._events[self._eventcounter].iteritems():
      for part in v:
        mom=part.vector()
        rmom=ROOT.TLorentzVector(mom[1],mom[2],mom[3],mom[0])
        particles.append(Particle(k,rmom,part['ntrk']))
    self._eventcounter+=1
    self._totaleventcounter+=1
    return Event(1.0,particles)

def ptmiss():
  pass

def isMuon(part):
  ids=['muon',13,-13]
  return (part.id in ids)

def isElectron(part):
  ids=[11,-11,'electron']
  return (part.id in ids)

def isJet(part):
  ids=['jet']
  return (part.id in ids)

def isInvis(part):
  ids=['MET']
  return (part.id in ids)

def isLepton(part):
  lepids=[11,13,-11,-13,'electron','muon']
  return (part.id in lepids)

#def leadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isLepton(p)]
#  if len(mupt)>0: return max(mupt)
#  else: return 0.0
#def leadingPTMuonCut(evt):
#  return leadingPTMuon(evt) > 25.0

#def subleadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isLepton(p)]
#  mupt.sort()
#  if len(mupt)>1: return mupt[-2]
#  else: return 0.0
#def subleadingPTMuonCut(evt):
#  return subleadingPTMuon(evt) > 20.0

def leadingPTMuon(evt):
  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
  if len(mupt)>0: return max(mupt)
  else: return 0.0
def subleadingPTMuon(evt):
  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
  mupt.sort()
  if len(mupt)>1: return mupt[-2]
  else: return 0.0
def leadingPTElectron(evt):
  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
  if len(ept)>0: return max(ept)
  else: return 0.0
def subleadingPTElectron(evt):
  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
  ept.sort()
  if len(ept)>1: return ept[-2]
  else: return 0.0
def leadingPTLeptonCut(evt):
  maxpTmuon = leadingPTMuon(evt)
  maxpTElectron = leadingPTElectron(evt)
  if maxpTmuon > maxpTElectron: return maxpTmuon > 20.0
  else: return maxpTElectron > 25.0
def subleadingPTLeptonCut(evt):
  maxpTmuon = leadingPTMuon(evt)
  maxpTElectron = leadingPTElectron(evt)
  submaxpTmuon = subleadingPTMuon(evt)
  submaxpTElectron = subleadingPTElectron(evt)
  if maxpTmuon > maxpTElectron: return submaxpTmuon > 20.0
  else: return submaxpTElectron > 20.0

#def leadingPTMuonCut(evt):
#  return leadingPTMuon(evt) > 20.0
#
#def subleadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
#  mupt.sort()
#  if len(mupt)>1: return mupt[-2]
#  else: return 0.0
#def subleadingPTMuonCut(evt):
#  return subleadingPTMuon(evt) > 20.0

#def leadingPTElectron(evt):
#  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
#  if len(ept)>0: return max(ept)
#  else: return 0.0
#def leadingPTElectronCut(evt):
#  return leadingPTElectron(evt) > 25.0
#
#def subleadingPTElectron(evt):
#  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
#  ept.sort()
#  if len(ept)>1: return ept[-2]
#  else: return 0.0
#def subleadingPTElectronCut(evt):
#  return subleadingPTElectron(evt) > 20.0

def numberLeptons(evt):
  return len([p for p in evt.particles if isLepton(p)])
def numberLeptonsCut(evt):
  leps=[part for part in evt.particles if isLepton(part) and (part.p.Pt()>10. if isElectron(part) else True) and (part.p.Pt()>5. if isMuon(part) else True)]
  return len(leps)==2 and sum(l.charge for l in leps)==0 and (all(isMuon(l) for l in leps) or all(isElectron(l) for l in leps))

def invMassZWindow(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0 
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom.M()
def invMassZWindowCut(evt):
  return abs(invMassZWindow(evt)-91.2)<15.0

def pTll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom.Pt()
def pTllCut(evt):
  return pTll(evt) > 60.0

def secondJetPt(evt):
  jets=[part for part in evt.particles if isJet(part)]
  if len(jets)<2: return 0.0
  jets.sort(key=lambda part: part.p.Pt())
  return jets[-2].p.Pt()
def secondJetPtCut(evt):
  return secondJetPt(evt) < 30.0

def missPt(evt):
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return totalmom.Pt()
def missPtCut(evt):
  return missPt(evt) > 100.0

def pTmissminuspTlloverpTll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return abs(totalmom.Pt()-putativeZmom.Pt())/putativeZmom.Pt()
def pTmissminuspTlloverpTllCut(evt):
  return pTmissminuspTlloverpTll(evt) < 0.4

def deltaRll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  return pl1.DeltaR(pl2)
def deltaRllCut(evt):
  return deltaRll(evt) < 1.8

def deltaphillmiss(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0 
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return totalmom.DeltaPhi(putativeZmom)
def deltaphillmissCut(evt):
  return deltaphillmiss(evt) > 2.6

def deltaphijetmiss(evt):
  jets=[part for part in evt.particles if isJet(part) and part.p.Pt()>30.]
  if len(jets)<1: return 3.1 
  jets.sort(key=lambda part: part.p.Pt())
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return jets[-1].p.DeltaPhi(totalmom)
def deltaphijetmissCut(evt):
  return deltaphijetmiss(evt) > 0.5



cutFuncs=[
numberLeptonsCut,
leadingPTLeptonCut,
subleadingPTLeptonCut,
#leadingPTMuonCut,
#subleadingPTMuonCut,
#leadingPTElectronCut,
#subleadingPTElectronCut,
invMassZWindowCut,
secondJetPtCut,
pTllCut,
missPtCut,
deltaphijetmissCut,
deltaphillmissCut,
pTmissminuspTlloverpTllCut,
deltaRllCut
]



def ZZreweight(evt):
  adjweight = evt.weight*(0.98-pTll(evt)*0.38/700)
  if deltaphillmiss(evt) < 1.5: adjweight*= 1.25
  if (deltaphillmiss(evt) > 1.5 and deltaphillmiss(evt) < 2): adjweight*=1.21
  if (deltaphillmiss(evt) > 2.0 and deltaphillmiss(evt) < 2.25): adjweight*=1.18
  if (deltaphillmiss(evt) > 2.25 and deltaphillmiss(evt) < 2.5): adjweight*=1.15
  if (deltaphillmiss(evt) > 2.5 and deltaphillmiss(evt) < 2.75): adjweight*=1.09
  if (deltaphillmiss(evt) > 2.75 and deltaphillmiss(evt) < 3): adjweight*=0.9
  if (deltaphillmiss(evt) > 3 and deltaphillmiss(evt) < 3.25): adjweight*=1.01
  return adjweight

def WZreweight(evt):
  return evt.weight*1.109*1.03

def unitReweight(evt):
  return evt.weight

def ZdecayReweight(evt):
  return 0.0673*evt.weight




def dileptonMom(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom

sampleDicts=collections.OrderedDict([
  ('WZ',{'reweightFunc':WZreweight,'filename':['scratchResult/WZ/WZsamp{0}/WZsamp{0}events.lhco'.format(i) for i in range(10)]})
#  ('DMsimpspin1_500_150',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed500dm150events.lhco'})
#  ('DMsimpspin1_500_300',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed500dm300events.lhco'}),
#  ('DMsimpspin1_1000_150',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed1000dm150events.lhco'}),
#  ('DMsimpspin1_1000_300',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed1000dm300events.lhco'})
#  ('DMsimpspin1_1000_100',{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1DECAY/med1000dm1000/med1000dm1000events.lhco'})
])

def outputSlimmedSamples(sampleDicts):
  for i,lbl in enumerate(sampleDicts):
    fname=sampleDicts[lbl]['filename']
    reader=LHCOEventIterator(fname)
    passedEvents=[evt for evt in reader if all(cf(evt) for cf in cutFuncs)]
    pts=np.array([missPt(evt) for evt in passedEvents])
    ys=np.array([abs(dileptonMom(evt).Rapidity()) for evt in passedEvents])
    weights=np.array([sampleDicts[lbl]['reweightFunc'](evt) for evt in passedEvents])
    xsec=np.array( [ reader.xsec * len(passedEvents)/reader._totaleventcounter] )
    np.savez(lbl+'_slimmed.npz',pts=pts,ys=ys,weights=weights,xsec=xsec)
  return

def readInSlimmedSamples(sampleDicts):
  for lbl in sampleDicts:
    sample=np.load(lbl+'_slimmed.npz')
    print sample['pts']
    print sample['ys']
    print sample['weights']
    print sample['xsec'][0]


if __name__=='__main__':
  outputSlimmedSamples(sampleDicts)
  #readInSlimmedSamples(sampleDicts)
