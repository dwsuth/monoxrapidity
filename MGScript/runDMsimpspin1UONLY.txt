launch DMsimpspin1UONLY
shower=PYTHIA8
madspin=ON
#set gVd31 0.001
set my1 500
set mxd 150
set nevents 10000
set ptl 0.0
set ptj 0.0
set mll_sf 0.0
set jetalgo -1
set jetradius 0.4
edit madspin --replace_line="decay z" decay z > l+ l-
edit madspin --after_line="decay z" decay y1 > xd xd~
#edit shower --replace_line="EXTRALIBS" EXTRALIBS    = stdhep Fmcfio dl  # Extra-libraries (not LHAPDF)
