#!/usr/bin/env python

import os
import re
import math
import collections


from array import array
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import ROOT
from ROOT import TH1F

from LHCOTools import *




fig4block1=np.array([12.57012177190215,
6.662839069977674,
3.9129791949992634,
2.3572541742020854,
1.1141647740493739,
0.4251309801337883,
0.20086436450478787,
0.10243953269878416,
0.0375098814166879,
0.017869525529223346])

fig4block2=np.array([7.3050517536931014,
2.656443228016665,
1.3072176148484307,
0.7485885295890271,
0.30070859015933926,
0.08261718212126022,
0.042646512751529,
0.022864339046333233,
0.005057557849374628,
0.002836246517746987])

fig4block3=np.array([4.58169857737925,
0.853634065514012,
0.38950150181974974,
0.16898842009398155,
0.020750886943415786,
0.0048398539593067225,
0.0,
0.0,
0.0,
0.0])

ZZfig4heights=fig4block1-fig4block2
WZfig4heights=fig4block2-fig4block3

zhinvfig4heights=[
0.4795166518476471,
0.5421695683850917,
0.45330408700807845,
0.30195164029153265,
0.1560762664412512,
0.06263015704884382,
0.025105802078270362,
0.010598010163001433,
0.00408200430255612,
0.0026615437757708097]

DMsimpfig4heights=[
1.445731751817266,
1.2742749857031372,
1.002527954257903,
0.6778605362964779,
0.3989120846025514,
0.20691380811147986,
0.10076030505869185,
0.048451476651465084,
0.025774141622340308,
0.013538761800225556]

def makeFig4():
  ##define intrinsic properties of fig 4
  intluminosity=35900.
  xbinedges=[100.,125.,150.,175.,200.,250.,300.,350.,400.,500.,600.]
  ##use sampleDicts - a dictionary of dictionaries - to store the relevant variables for each sample of interest
  sampleDicts=collections.OrderedDict({
    'xqcut7p5':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1UONLYDECAYjet/xqcut7p5/xqcut7p5events.lhco'},
    'xqcut15':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1UONLYDECAYjet/xqcut15/xqcut15events.lhco'},
    'xqcut30':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1UONLYDECAYjet/xqcut30/xqcut30events.lhco'},
   # 'unmerged':{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed500dm150events.lhco'}
    'xqcut60':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1UONLYDECAYjet/xqcut60/xqcut60events.lhco'},
    'xqcut120':{'reweightFunc':ZdecayReweight,'filename':'scratchResult/DMsimpspin1UONLYDECAYjet/xqcut120/xqcut120events.lhco'}
  })
  

  for lbl in sampleDicts:
    eventadjCount=0
    passadjCount=0
    totweight=0.
    totadjweight=0.
    reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
    newhistadj=TH1F(lbl+'_adj',lbl+'_adj',len(xbinedges)-1,array('d',xbinedges))
    for evt in reader:
      if all(cutfunc(evt) for cutfunc in cutFuncs):
        passadjCount+=1
        evtadjweight=sampleDicts[lbl]['reweightFunc'](evt)
        totweight+=evt.weight
        totadjweight+=evtadjweight
        newhistadj.Fill(missPt(evt),evtadjweight)  
      eventadjCount+=1
    sampleDicts[lbl]['totweight']=totweight
    sampleDicts[lbl]['totadjweight']=totadjweight
    newhistadj.Scale(intluminosity*reader.xsec/eventadjCount,"width")
    newhistadj.SetXTitle('ptmiss [GeV]')
    newhistadj.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['adjhist']=newhistadj

  ##draw adjusted histgrams
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['adjhist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj.pdf')
  
  ####plot ratios
  for lbl in sampleDicts:
    dmsimphist=sampleDicts[lbl]['adjhist']
    for i in range(len(DMsimpfig4heights)):
      dmsimphist.SetBinContent(i+1,dmsimphist.GetBinContent(i+1)/DMsimpfig4heights[i])
      dmsimphist.SetBinError(i+1,dmsimphist.GetBinError(i+1)/DMsimpfig4heights[i])
    dmsimphist.Draw()
    ROOT.gPad.SaveAs(lbl+'ratiofig4.pdf')
  ####plot ratiosall on one plot
  leg=ROOT.TLegend(0.8,0.8,1.0,1.0)
  for i,lbl in enumerate(sampleDicts):
    dmsimphist=sampleDicts[lbl]['adjhist']
    dmsimphist.SetLineColor(i+2)
    leg.AddEntry(dmsimphist,lbl,'L')
    if i==0: dmsimphist.Draw()
    else: dmsimphist.Draw('same')
  leg.Draw()
  ROOT.gPad.SaveAs('xqcutratiofig4.pdf')
  return sampleDicts


if __name__=='__main__':
  sampleDicts=makeFig4()
