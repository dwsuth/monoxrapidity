#!/usr/bin/env python

import os
#import lhereader
import re
import math
import collections
#import hepmcio
from LHCO_reader import LHCO_reader


from array import array
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

import ROOT
from ROOT import TH1F

targetdirectory='/home/tom/MG5_aMC_v2_6_3_2'

class Event:
  def __init__(self,weight,particles):
    self.weight=weight
    self.adjweight = weight
    self.particles=particles
#    self.xsec = xsec

class Particle:
  def __init__(self,pid,mom,charge):
    self.id=pid
    self.p=mom
    if charge is None:
      if self.id in [11,13,15]: self.charge=-1
      elif self.id in [-11,-13,-15]: self.charge=1
      else: self.charge=0
    else: self.charge=charge
    return

class EventIterator:
  def __init__(self,eventfile):
    self.reader=hepmcio.HepMCReader(os.path.join(targetdirectory,'Events',eventfile))
  def __iter__(self): return self
  def next(self):
    hepmcevt=self.reader.next()
    if not hepmcevt:
      raise StopIteration
    particles=[Particle(part.pid,ROOT.TLorentzVector(*tuple(part.mom)),None) for part in hepmcevt.particles.values()]
    if len(hepmcevt.weights)!=1: raise Exception
    return Event(hepmcevt.weights[0],particles)
    
class LHCOEventIterator:
  def getXSec(self,eventfile):
    with open(eventfile,'r') as f:
      text=f.read()
    stuff=re.search(r'Integrated weight \(pb\)  : (\S*)',text)
    return float(stuff.group(1))
  def __init__(self,eventfiles,maxEvents=None):
    if not isinstance(eventfiles,basestring):
      self._eventfilenames=eventfiles
    else: self._eventfilenames=[eventfiles]
    self._filecounter = 0
    self._eventcounter = 0
    self._totaleventcounter = 0
    self._maxEvents=maxEvents
    ##preload first event file
    print 'Loading ',self._eventfilenames[self._filecounter]
    self._events = LHCO_reader.Events(f_name=self._eventfilenames[self._filecounter],n_events=self._maxEvents)
    ##get cross section from first event file
    self.xsec=self.getXSec(self._eventfilenames[0])
  def __iter__(self): return self
  def next(self):
    if self._eventcounter>=len(self._events):
      if self._maxEvents is not None and self._eventcounter>=self._maxEvents: raise StopIteration
      if self._filecounter+1>=len(self._eventfilenames): raise StopIteration
      else:
        self._filecounter+=1
        self._eventcounter=0
        print 'Loading ',self._eventfilenames[self._filecounter]
        self._events = LHCO_reader.Events(f_name=self._eventfilenames[self._filecounter],n_events=(self._maxEvents-self._totaleventcounter if self._maxEvents else None))
    particles=[]
    for k,v in self._events[self._eventcounter].iteritems():
      for part in v:
        mom=part.vector()
        rmom=ROOT.TLorentzVector(mom[1],mom[2],mom[3],mom[0])
        particles.append(Particle(k,rmom,part['ntrk']))
    self._eventcounter+=1
    self._totaleventcounter+=1
    return Event(1.0,particles)

def ptmiss():
  pass

def isMuon(part):
  ids=['muon',13,-13]
  return (part.id in ids)

def isElectron(part):
  ids=[11,-11,'electron']
  return (part.id in ids)

def isJet(part):
  ids=['jet']
  return (part.id in ids)

def isInvis(part):
  ids=['MET']
  return (part.id in ids)

def isLepton(part):
  lepids=[11,13,-11,-13,'electron','muon']
  return (part.id in lepids)

#def leadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isLepton(p)]
#  if len(mupt)>0: return max(mupt)
#  else: return 0.0
#def leadingPTMuonCut(evt):
#  return leadingPTMuon(evt) > 25.0

#def subleadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isLepton(p)]
#  mupt.sort()
#  if len(mupt)>1: return mupt[-2]
#  else: return 0.0
#def subleadingPTMuonCut(evt):
#  return subleadingPTMuon(evt) > 20.0

def leadingPTMuon(evt):
  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
  if len(mupt)>0: return max(mupt)
  else: return 0.0
def subleadingPTMuon(evt):
  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
  mupt.sort()
  if len(mupt)>1: return mupt[-2]
  else: return 0.0
def leadingPTElectron(evt):
  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
  if len(ept)>0: return max(ept)
  else: return 0.0
def subleadingPTElectron(evt):
  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
  ept.sort()
  if len(ept)>1: return ept[-2]
  else: return 0.0
def leadingPTLeptonCut(evt):
  maxpTmuon = leadingPTMuon(evt)
  maxpTElectron = leadingPTElectron(evt)
  if maxpTmuon > maxpTElectron: return maxpTmuon > 20.0
  else: return maxpTElectron > 25.0
def subleadingPTLeptonCut(evt):
  maxpTmuon = leadingPTMuon(evt)
  maxpTElectron = leadingPTElectron(evt)
  submaxpTmuon = subleadingPTMuon(evt)
  submaxpTElectron = subleadingPTElectron(evt)
  if maxpTmuon > maxpTElectron: return submaxpTmuon > 20.0
  else: return submaxpTElectron > 20.0

#def leadingPTMuonCut(evt):
#  return leadingPTMuon(evt) > 20.0
#
#def subleadingPTMuon(evt):
#  mupt=[p.p.Pt() for p in evt.particles if isMuon(p)]
#  mupt.sort()
#  if len(mupt)>1: return mupt[-2]
#  else: return 0.0
#def subleadingPTMuonCut(evt):
#  return subleadingPTMuon(evt) > 20.0

#def leadingPTElectron(evt):
#  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
#  if len(ept)>0: return max(ept)
#  else: return 0.0
#def leadingPTElectronCut(evt):
#  return leadingPTElectron(evt) > 25.0
#
#def subleadingPTElectron(evt):
#  ept=[p.p.Pt() for p in evt.particles if isElectron(p)]
#  ept.sort()
#  if len(ept)>1: return ept[-2]
#  else: return 0.0
#def subleadingPTElectronCut(evt):
#  return subleadingPTElectron(evt) > 20.0

def numberLeptons(evt):
  return len([p for p in evt.particles if isLepton(p)])
def numberLeptonsCut(evt):
  leps=[part for part in evt.particles if isLepton(part) and (part.p.Pt()>10. if isElectron(part) else True) and (part.p.Pt()>5. if isMuon(part) else True)]
  return len(leps)==2 and sum(l.charge for l in leps)==0 and (all(isMuon(l) for l in leps) or all(isElectron(l) for l in leps))

def invMassZWindow(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0 
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom.M()
def invMassZWindowCut(evt):
  return abs(invMassZWindow(evt)-91.2)<15.0

def pTll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom.Pt()
def pTllCut(evt):
  return pTll(evt) > 60.0

def secondJetPt(evt):
  jets=[part for part in evt.particles if isJet(part)]
  if len(jets)<2: return 0.0
  jets.sort(key=lambda part: part.p.Pt())
  return jets[-2].p.Pt()
def secondJetPtCut(evt):
  return secondJetPt(evt) < 30.0

def missPt(evt):
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return totalmom.Pt()
def missPtCut(evt):
  return missPt(evt) > 100.0

def pTmissminuspTlloverpTll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return abs(totalmom.Pt()-putativeZmom.Pt())/putativeZmom.Pt()
def pTmissminuspTlloverpTllCut(evt):
  return pTmissminuspTlloverpTll(evt) < 0.4

def deltaRll(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  return pl1.DeltaR(pl2)
def deltaRllCut(evt):
  return deltaRll(evt) < 1.8

def deltaphillmiss(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  if len(leptons)<2: return 0.0 
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return totalmom.DeltaPhi(putativeZmom)
def deltaphillmissCut(evt):
  return deltaphillmiss(evt) > 2.6

def deltaphijetmiss(evt):
  jets=[part for part in evt.particles if isJet(part) and part.p.Pt()>30.]
  if len(jets)<1: return 3.1 
  jets.sort(key=lambda part: part.p.Pt())
  invismomenta=[part.p for part in evt.particles if isInvis(part)]
  totalmom=sum(invismomenta,ROOT.TLorentzVector())
  return jets[-1].p.DeltaPhi(totalmom)
def deltaphijetmissCut(evt):
  return deltaphijetmiss(evt) > 0.5


#events=LHCOEventIterator('testevts.lhco')
#for e in events:
#  print len(e.particles)
#
#sys.exit(0)
#events=LHCO_reader.Events(f_name='testevts.lhco')
#for e in events:
#  print '**'
#  for k,v in e.iteritems():
#    print k,len(v)
#    
#  for l in e['electron']:
#    print l['PT']

#ROOT.gSystem.Load('libExRootAnalysis')
#ROOT.gSystem.Load('libDelphes')
#myfile = ROOT.TFile("testevts.root")
#mytree = myfile.Delphes
#for event in mytree:
#  for jet in event.Jet:
#    print jet.px


histDicts=[
#{'func':leadingPTMuon,'range':(100,0,100)},
#{'func':subleadingPTMuon,'range':(100,0,100)},
#{'func':leadingPTLepton,'range':(100,0,100)},
#{'func':subleadingLepton,'range':(100,0,100)},
#{'func':leadingPTElectron,'range':(100,0,100)},
#{'func':subleadingPTElectron,'range':(100,0,100)},
{'func':numberLeptons,'range':(5,0,5)},
{'func':invMassZWindow,'range':(100,0,300)},
{'func':pTll,'range':(100,0,300)},
{'func':secondJetPt,'range':(100,0,300)},
{'func':missPt,'range':(100,0,600)},
{'func':pTmissminuspTlloverpTll,'range':(100,0,3.0)},
{'func':deltaRll,'range':(100,0,5.0)},
{'func':deltaphillmiss,'range':(100,0,3.2)},
{'func':deltaphijetmiss,'range':(100,0,3.2)}
]

cutFuncs=[
numberLeptonsCut,
leadingPTLeptonCut,
subleadingPTLeptonCut,
#leadingPTMuonCut,
#subleadingPTMuonCut,
#leadingPTElectronCut,
#subleadingPTElectronCut,
invMassZWindowCut,
secondJetPtCut,
pTllCut,
missPtCut,
deltaphijetmissCut,
deltaphillmissCut,
pTmissminuspTlloverpTllCut,
deltaRllCut
]

#def kfactors(events)

def cutFlow(cutfuncs,events):
  for cf in cutfuncs:
    events=[e for e in events if cf(e)]
    print cf.__name__,len(events)
  return

def plotCutVariables():
  ##initialise hists
  for d in histDicts:
    d['hist']=TH1F(*( (d['func'].__name__,d['func'].__name__) + d['range'] ))
    print d['hist']

  ##iterate over events
  reader=LHCOEventIterator('testevts.lhco')
  for i,e in enumerate(reader):
    if i % 1000 ==0: print "Event no.",i
    for hd in histDicts:
      hd['hist'].Fill(hd['func'](e),e.weight)

  ##draw hists
  for hd in histDicts:
    hd['hist'].Draw()
    ROOT.gPad.SaveAs(hd['func'].__name__+'.pdf')
  return

fig4block1=np.array([12.57012177190215,
6.662839069977674,
3.9129791949992634,
2.3572541742020854,
1.1141647740493739,
0.4251309801337883,
0.20086436450478787,
0.10243953269878416,
0.0375098814166879,
0.017869525529223346])

fig4block2=np.array([7.3050517536931014,
2.656443228016665,
1.3072176148484307,
0.7485885295890271,
0.30070859015933926,
0.08261718212126022,
0.042646512751529,
0.022864339046333233,
0.005057557849374628,
0.002836246517746987])

fig4block3=np.array([4.58169857737925,
0.853634065514012,
0.38950150181974974,
0.16898842009398155,
0.020750886943415786,
0.0048398539593067225,
0.0,
0.0,
0.0,
0.0])

ZZfig4heights=fig4block1-fig4block2
WZfig4heights=fig4block2-fig4block3

zhinvfig4heights=[
0.4795166518476471,
0.5421695683850917,
0.45330408700807845,
0.30195164029153265,
0.1560762664412512,
0.06263015704884382,
0.025105802078270362,
0.010598010163001433,
0.00408200430255612,
0.0026615437757708097]

DMsimpfig4heights=[
1.445731751817266,
1.2742749857031372,
1.002527954257903,
0.6778605362964779,
0.3989120846025514,
0.20691380811147986,
0.10076030505869185,
0.048451476651465084,
0.025774141622340308,
0.013538761800225556]

def ZZreweight(evt):
  adjweight = evt.weight*(0.98-pTll(evt)*0.38/700)
  if deltaphillmiss(evt) < 1.5: adjweight*= 1.25
  if (deltaphillmiss(evt) > 1.5 and deltaphillmiss(evt) < 2): adjweight*=1.21
  if (deltaphillmiss(evt) > 2.0 and deltaphillmiss(evt) < 2.25): adjweight*=1.18
  if (deltaphillmiss(evt) > 2.25 and deltaphillmiss(evt) < 2.5): adjweight*=1.15
  if (deltaphillmiss(evt) > 2.5 and deltaphillmiss(evt) < 2.75): adjweight*=1.09
  if (deltaphillmiss(evt) > 2.75 and deltaphillmiss(evt) < 3): adjweight*=0.9
  if (deltaphillmiss(evt) > 3 and deltaphillmiss(evt) < 3.25): adjweight*=1.01
  return adjweight

def WZreweight(evt):
  return evt.weight*1.109*1.03

def unitReweight(evt):
  return evt.weight

def ZdecayReweight(evt):
  return 0.0673*evt.weight

def makeFig4():
  ##define intrinsic properties of fig 4
  intluminosity=35900.
  xbinedges=[100.,125.,150.,175.,200.,250.,300.,350.,400.,500.,600.]
  ##use sampleDicts - a dictionary of dictionaries - to store the relevant variables for each sample of interest
  sampleDicts={
    'ZZ':{'reweightFunc':ZZreweight,'filename':'ZZsamp0events.lhco'},
    'WZ':{'reweightFunc':WZreweight,'filename':'WZevents.lhco'},
    'DMsimpspin1':{'reweightFunc':ZdecayReweight,'filename':'DMknotevents.lhco'}
  }
  
  ##fill individual histograms
  for lbl in sampleDicts:
    reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
    newhist=TH1F(lbl,lbl,len(xbinedges)-1,array('d',xbinedges))
    eventCount=0
    passCount=0
    for evt in reader:
      if all(cutfunc(evt) for cutfunc in cutFuncs):
        newhist.Fill(missPt(evt),evt.weight)
        passCount+=1
      eventCount+=1
    print '{0} of {1} events of {2} sample pass the cuts, SR xsec {3} pb, SR events {4}'.format(passCount,eventCount,lbl,reader.xsec*passCount/eventCount,intluminosity*reader.xsec*passCount/eventCount)
    newhist.Scale(intluminosity*reader.xsec/eventCount,"width")
    newhist.SetXTitle('ptmiss [GeV]')
    newhist.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['hist']=newhist
    sampleDicts[lbl]['xsec']=reader.xsec
    sampleDicts[lbl]['eventCount']=eventCount
    sampleDicts[lbl]['passCount']=passCount

  ## draw individual histograms
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['hist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['hist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4.pdf')

  ##k-factor adj
  print ''
  print 'After k-factors:'
  print 'The following results after k-factors are applied should have the same number of each type of events passing the cuts - as we have not changed any kinematic variables just rescaled their weights, the histograms however change as different k-factors are applied in different regions, meanwhile the SR region events and xsec are then scaled according to the new total passed cuts event weights over the previous before k factor adjustments'

  for lbl in sampleDicts:
    eventadjCount=0
    passadjCount=0
    totweight=0.
    totadjweight=0.
    reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
    newhistadj=TH1F(lbl+'_adj',lbl+'_adj',len(xbinedges)-1,array('d',xbinedges))
    for evt in reader:
      if all(cutfunc(evt) for cutfunc in cutFuncs):
        passadjCount+=1
        evtadjweight=sampleDicts[lbl]['reweightFunc'](evt)
        totweight+=evt.weight
        totadjweight+=evtadjweight
        newhistadj.Fill(missPt(evt),evtadjweight)  
      eventadjCount+=1
    sampleDicts[lbl]['totweight']=totweight
    sampleDicts[lbl]['totadjweight']=totadjweight
    newhistadj.Scale(intluminosity*reader.xsec/eventadjCount,"width")
    newhistadj.SetXTitle('ptmiss [GeV]')
    newhistadj.SetYTitle('dN/dpT [GeV^-1]')
    sampleDicts[lbl]['adjhist']=newhistadj
    print '{0} of {1} events of {4} sample pass the cuts, SR xsec {2} pb, SR events {3}'.format(sampleDicts[lbl]['passCount'],sampleDicts[lbl]['eventCount'],reader.xsec*totadjweight/totweight*sampleDicts[lbl]['passCount']/sampleDicts[lbl]['eventCount'],intluminosity*reader.xsec*totadjweight/totweight*sampleDicts[lbl]['passCount']/sampleDicts[lbl]['eventCount'],lbl)

  ##draw adjusted histgrams
  for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist'].SetFillColor(i+2)
  for lbl in sampleDicts:
    hist=sampleDicts[lbl]['adjhist']
    print 'bin entries',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
    hist.Draw("hist")
    print 'Histogram integral for {0} = {1}'.format(lbl,hist.Integral())
    ROOT.gPad.SetLogy();
    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj.pdf')

  ##make stacked histogram
  fig4stack=ROOT.THStack('fig4','fig4')
  for lbl in sampleDicts: fig4stack.Add(sampleDicts[lbl]['hist'])
  fig4stack.Draw("hist")
  ROOT.gPad.SaveAs('fig4.pdf')

  ##make k-factor adjusted stacked histogram
  fig4stackadj=ROOT.THStack('fig4adj','fig4adj')
  for lbl in sampleDicts: fig4stackadj.Add(sampleDicts[lbl]['adjhist'])
  fig4stackadj.Draw("hist")
  ROOT.gPad.SaveAs('fig4kfactoradj.pdf')
  
  ####plot ratios
  wzhist=sampleDicts['WZ']['hist']
  zzhist=sampleDicts['ZZ']['hist']
  dmsimphist=sampleDicts['DMsimpspin1']['adjhist']
  for i in range(len(WZfig4heights)):
    wzhist.SetBinContent(i+1,wzhist.GetBinContent(i+1)/WZfig4heights[i])
  wzhist.Draw()
  ROOT.gPad.SaveAs('ratioswzfig4.pdf')
  for i in range(len(ZZfig4heights)):
    zzhist.SetBinContent(i+1,zzhist.GetBinContent(i+1)/ZZfig4heights[i])
  zzhist.Draw()
  ROOT.gPad.SaveAs('ratioszzfig4.pdf')
  for i in range(len(DMsimpfig4heights)):
    dmsimphist.SetBinContent(i+1,dmsimphist.GetBinContent(i+1)/DMsimpfig4heights[i])
  dmsimphist.Draw()
  ROOT.gPad.SaveAs('ratiosdmsimpfig4.pdf')
####k-factor adjusted fig4
#  wzhistadj=next(h for h in hists if h.GetName()=='WZ')
#  zzhistadj=next(h for h in hists if h.GetName()=='ZZ')
#  DMsimphistadj=next(h for h in hists if h.GetName()=='DMsimpspin1')
#  for i in range(len(WZfig4heights)):
#    wzhistadj.SetBinContent(i, wzhistadj.GetBinContent(i)*1.109*1.03)
#  wzhistadj.Fill() 
#  wzhistadj.Draw()
#  ROOT.gPad.SaveAs('wzkfactoradjfig4.pdf')
#  for i in range(len(ZZfig4heights)):
#    if zzhistlog.GetBinContent(i)<1e-10: zzhistlog.SetBinContent(i,1e-10)
#    zzhistlog.SetBinContent(i, math.log10(zzhistlog.GetBinContent(i)))
#  zzhistlog.Draw()
#  ROOT.gPad.SaveAs('logzzfig4.pdf')
#  for i in range(len(DMsimpfig4heights)):
#    if DMsimphistlog.GetBinContent(i)<1e-10: DMsimphistlog.SetBinContent(i,1e-10)
#    DMsimphistlog.SetBinContent(i, math.log10(DMsimphistlog.GetBinContent(i)))
#  DMsimphistlog.Draw()
#  ROOT.gPad.SaveAs('logDMsimpfig4.pdf')
#  logfig4hist=next(h for h in hists)
#  for i in range(len(WZfig4heights)):
#     logfig4hist.SetBinContent(i,wzhistlog.GetBinContent(i)+zzhistlog.GetBinContent(i)+DMsimphistlog.GetBinContent(i))
#  logfig4hist.Draw()
#  ROOT.gPad.SaveAs('logfig4hist.pdf')

  
  return sampleDicts


def plotCutVariables():
  ##initialise hists
  for d in histDicts:
    d['hist']=TH1F(*( (d['func'].__name__,d['func'].__name__) + d['range'] ))
    print d['hist']

  ##iterate over events
  reader=LHCOEventIterator('testevts.lhco')
  for i,e in enumerate(reader):
    if i % 1000 ==0: print "Event no.",i
    for hd in histDicts:
      hd['hist'].Fill(hd['func'](e),e.weight)

  ##draw hists
  for hd in histDicts:
    hd['hist'].Draw()
    ROOT.gPad.SaveAs(hd['func'].__name__+'.pdf')
  return


def doCutFlow():
  labels=['ZZ','WZ','WW','DMsimpspin1']
  filenames=[lbl+'events.lhco' for lbl in labels]
  eventss=[LHCOEventIterator(fname) for fname in filenames]
  for lbl,events in zip(labels,eventss):
    print '***********',lbl,'*************'
    cutFlow(cutFuncs,events)
  return

def dileptonMom(evt):
  leptons=[part for part in evt.particles if isLepton(part)]
  leptons.sort(key=lambda part: part.p.Pt())
  pl1=leptons[-2].p
  pl2=leptons[-1].p
  putativeZmom=pl1+pl2
  return putativeZmom

def plotPtY():
  sampleDicts=collections.OrderedDict([
    ('ZZ',{'reweightFunc':ZZreweight,'filename':['ZZsamp{0}events.lhco'.format(i) for i in range(10)]}),
    ('DMsimpspin1_500_150',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed500dm150events.lhco'}),
    ('DMsimpspin1_500_300',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed500dm300events.lhco'}),
    ('DMsimpspin1_1000_150',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed1000dm150events.lhco'}),
    ('DMsimpspin1_1000_300',{'reweightFunc':ZdecayReweight,'filename':'DMsimpspin1DECAYmed1000dm300events.lhco'})
  ])
  fig,axes=plt.subplots(ncols=4,nrows=len(sampleDicts),figsize=(15,15))
  twodbins=[]
  labels=[]
  for i,lbl in enumerate(sampleDicts):
    labels.append(lbl)
    fname=sampleDicts[lbl]['filename']
    passedEvents=[evt for evt in LHCOEventIterator(fname) if all(cf(evt) for cf in cutFuncs)]
    pts=[dileptonMom(evt).Pt() for evt in passedEvents]       
    ys=[abs(dileptonMom(evt).Rapidity()) for evt in passedEvents]
    axes[i][0].scatter(pts,ys,s=1.)#,range=[[0,500],[0,2.5]])
    h,ptbinedges,ybinedges,im=axes[i][1].hist2d(pts,ys,bins=10,range=[[0,500],[0,2.5]],normed=True,vmin=0.0,vmax=0.005,cmap='jet')
    fig.colorbar(im,ax=axes[i][1])
    twodbins.append(h)
    print twodbins[0]
    print twodbins[-1]
    print np.subtract(twodbins[-1],twodbins[0])
    im=axes[i][2].imshow(np.subtract(twodbins[-1],twodbins[0]).T,origin='lower',aspect='auto',extent=[0,500,0,2.5])
    fig.colorbar(im,ax=axes[i][2])
    im=axes[i][3].imshow(np.divide(twodbins[-1],np.sqrt(twodbins[0])).T,origin='lower',aspect='auto',extent=[0,500,0,2.5])
    fig.colorbar(im,ax=axes[i][3])
    for j in [0,1,2,3]:
      axes[i][j].set_xlabel('pT')
      axes[i][j].set_ylabel('|y|')
      axes[i][j].set_title(lbl)
  plt.tight_layout()
  plt.savefig('ptydist.pdf')
  #print 'aargh',twodbins[0][3]
  #plt.clf()
  #binheights=list(twodbins[0][3])
  #binheights=[binheights[0]]+binheights
  #plt.plot(y,binheights,ls='steps')
  #plt.savefig('testydist.pdf')
  plt.clf()
  noPtBins=len(ptbinedges)-1
  norows=next(i for i in range(int(math.sqrt(float(noPtBins))),noPtBins+1) if noPtBins % i ==0)
  print noPtBins,norows,noPtBins/norows
  fig,axes=plt.subplots(ncols=noPtBins//norows,nrows=norows,figsize=(10,10))
  for i,ax in enumerate(axes.flatten()):
    fudgedweights=[list(twod[i])+[0.0] for twod in twodbins]
    ax.hist([ybinedges]*len(twodbins),weights=fudgedweights,histtype='step',normed=True,label=labels)
    ax.set_title('pt [{0},{1}]'.format(ptbinedges[i],ptbinedges[i+1]))
  plt.legend()
  plt.savefig('ydist.pdf')
  plt.clf()
  noYBins=len(ybinedges)-1
  norows=next(i for i in range(int(math.sqrt(float(noYBins))),noYBins+1) if noYBins % i ==0)
  print noYBins,norows,noYBins/norows
  fig,axes=plt.subplots(ncols=noYBins//norows,nrows=norows)
  for i,ax in enumerate(axes.flatten()):
    fudgedweights=[list(twod[:,i])+[0.0] for twod in twodbins]
    ax.hist([ptbinedges]*len(twodbins),weights=fudgedweights,histtype='step',normed=True,label=labels)
    ax.set_title('y [{0},{1}]'.format(ybinedges[i],ybinedges[i+1]))
  plt.legend()
  plt.savefig('ptdist.pdf')
  return 

def poissonPseudoExp(exprates,nExp):
  return np.array([np.random.poisson(lam=np.array(exprates)) for i in range(nExp)])

def loglikelihood(data,exprates):
  binnedLLs=np.where(data>0,-exprates+data*np.log(exprates)-data*np.log(data)+data,-exprates)
#  print 'data', data
#  print 'binnedLLS', binnedLLs, 'summedbinnedLLs', np.sum(binnedLLs)
  return np.sum(binnedLLs)

def pValue(actualLL,pseudodataLL):
  if hasattr(actualLL,'__iter__'):
    return [float(sum(1 for pdLL in pseudodataLL if pdLL <= actLL))/len(pseudodataLL) for actLL in actualLL]
  else:
    return float(sum(1 for pdLL in pseudodataLL if pdLL <= actualLL))/len(pseudodataLL)

def fig4temptestPValue(sampleDicts):
  ###extract bin
  hist=sampleDicts['ZZ']['hist']
  zzbincontent1=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
  print 'zzbins:', zzbincontent1
  hist=sampleDicts['DMsimpspin1']['hist']
  dmsimpbincontent1=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
  print 'dmsimpbins:', dmsimpbincontent1
  bkgCentralValues1=np.array(zzbincontent1)
  print 'bkgCentralvalues1:', bkgCentralValues1
  sigCentralValues1=np.array(dmsimpbincontent1)
  print 'sigCentralvalues1:', sigCentralValues1
  pseudodata1=poissonPseudoExp(bkgCentralValues1+sigCentralValues1,10)
  print 'pseudodata1', pseudodata1

  ###extract bin
  hist=sampleDicts['ZZ']['adjhist']
  zzbincontent2=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
  print 'zzbins:', zzbincontent2
  hist=sampleDicts['DMsimpspin1']['adjhist']
  dmsimpbincontent2=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
  print 'dmsimpbins:', dmsimpbincontent2
  bkgCentralValues2=np.array(zzbincontent2)
  print 'bkgCentralvalues2:', bkgCentralValues2
  sigCentralValues2=np.array(dmsimpbincontent2)
  print 'sigCentralvalues2:', sigCentralValues2
  pseudodata2=poissonPseudoExp(bkgCentralValues2+sigCentralValues2,10)
  print 'pseudodata2', pseudodata2
  
  return

def fig4PValue(sampleDicts):
  ###extract bin
  hist=sampleDicts['ZZ']['adjhist']
  zzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print 'zzbins:', zzbincontent
  hist=sampleDicts['WZ']['adjhist']
  wzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print 'wzbins:', wzbincontent
  hist=sampleDicts['DMsimpspin1']['adjhist']
  dmsimpbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print 'dmsimpbins:', dmsimpbincontent
  bkgCentralValues=np.array(zzbincontent)+np.array(wzbincontent)
#  print 'bkgCentralvalues:', bkgCentralValues
  sigCentralValues=np.array(dmsimpbincontent)
#  print 'sigCentralvalues:', sigCentralValues
#  print 'bkgCentralvalues + sigCentralvalues:', bkgCentralValues+sigCentralValues
  ###generate 1000 pseudo datasets (1000 sets of integer bin values), assuming bkg+sig rates
#  pseudodata=poissonPseudoExp(bkgCentralValues,1000)
  pseudodata=poissonPseudoExp(bkgCentralValues+sigCentralValues,10000)
#  print 'pseudodata:', pseudodata
  ###compute the log likelihood that each dataset comes from the bkg+sig distribution
  pseudodataLogLikelihoods=[loglikelihood(data,bkgCentralValues+sigCentralValues) for data in pseudodata]
#  print 'pseudodataLogLikelihoods', pseudodataLogLikelihoods
  print "pseudodataLogLikelihoods mean = ", sum(pseudodataLogLikelihoods)/len(pseudodataLogLikelihoods)
  ###assume the actual data are measured to be just the central values of the bkg distribution, compute the likelihood that this data comes from bkg+sig  
  #bkgdiscreteValues=bkgCentralValues//1
  actualLogLikelihoods=[loglikelihood(bkgdiscreteValues,bkgCentralValues+sigCentralValues) for bkgdiscreteValues in poissonPseudoExp(bkgCentralValues,10)]
  print  'actualLogLikelihood', actualLogLikelihoods
  ###calculate p-value: the fraction of pseudodatasets which have a smaller log likelihood than the actual dataset
  print "Overall p value...."
  print pValue(actualLogLikelihoods,pseudodataLogLikelihoods)
  return

def fig4pTbinbybinPValues(sampleDicts):
  print "pT bin by bin p values...."
###extract bin
  hist=sampleDicts['ZZ']['adjhist']
  zzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print zzbincontent
  hist=sampleDicts['WZ']['adjhist']
  wzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print wzbincontent
  hist=sampleDicts['DMsimpspin1']['adjhist']
  dmsimpbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print dmsimpbincontent
  bkgCentralValues=np.array(zzbincontent)+np.array(wzbincontent)
  sigCentralValues=np.array(dmsimpbincontent)
  ###generate 1000 pseudodata bin values for each bin, assuming bkg+sig rates and compute the log likelihood that each dataset comes from the bkg+sig distribution
  bin_pvalues = []
  for i in range(1,hist.GetNbinsX()):
#	print "bkg Values = ", bkgCentralValues
#	print "bkg + sig Values = ", bkgCentralValues+sigCentralValues
	pseudodata=poissonPseudoExp(bkgCentralValues[i]+sigCentralValues[i],10000)
#	print "pseudodata = ", pseudodata
	pseudodataLogLikelihoods=[loglikelihood(data,bkgCentralValues[i]+sigCentralValues[i]) for data in pseudodata]
#	print "pseudodata bin by bin log likelihoods", pseudodataLogLikelihoods
	###assume the actual data are measured to be just the central values of the bkg distribution, compute the likelihood that this data comes from bkg+sig  
 	bkgdiscreteValues=bkgCentralValues//1	
     	actualLogLikelihood=loglikelihood(bkgdiscreteValues[i],bkgCentralValues[i]+sigCentralValues[i])
#	print "actual bin by bin log likelhoods", actualLogLikelihood
     	###calculate p-value: the fraction of pseudodatasets which have a smaller log likelihood than the actual dataset
	bin_pvalues.append(pValue(actualLogLikelihood,pseudodataLogLikelihoods))
  print "bin pvalues = ", bin_pvalues
  return


def fig4_Splitineta(sampleDicts, ysplit):
	intluminosity = 35900
	labels=['DMsimpspin1','ZZ','WZ']
	ptbinedges=[100.,125.,150.,175.,200.,250.,300.,350.,400.,500.,600.]
	noPtBins=len(ptbinedges)-1
	for lbl in sampleDicts:
		reader=LHCOEventIterator(sampleDicts[lbl]['filename'])
		newhistadj_loweta=TH1F(lbl+'_adj_loweta',lbl+'_adj_loweta',len(ptbinedges)-1,array('d',ptbinedges))
		newhistadj_higheta=TH1F(lbl+'_adj_higheta',lbl+'_adj_higheta',len(ptbinedges)-1,array('d',ptbinedges))
       		newhistadj_loweta.SetXTitle('ptmiss [GeV]')
		newhistadj_loweta.SetYTitle('dN/dpT [GeV^-1]')
		newhistadj_higheta.SetTitle('pT spectrum for low eta, |y| < 1.0')
        	newhistadj_higheta.SetXTitle('ptmiss [GeV]')
       		newhistadj_higheta.SetYTitle('dN/dpT [GeV^-1]')
		newhistadj_higheta.SetTitle('pT spectrum for high eta, |y| >= 1.0')
		sampleDicts[lbl]['adjhist_loweta']=newhistadj_loweta	
		sampleDicts[lbl]['adjhist_higheta']=newhistadj_higheta
	 	totweight = 0
		evtadjweight = 0
		totadjweight = 0
		eventadjCount = 0
		for evt in reader:
			if all(cutfunc(evt) for cutfunc in cutFuncs):
		        	evtadjweight=sampleDicts[lbl]['reweightFunc'](evt)
		        	totweight+=evt.weight
		        	totadjweight+=evtadjweight
				if abs(dileptonMom(evt).Rapidity()) < ysplit:
				        newhistadj_loweta.Fill(missPt(evt),evtadjweight)  
#					print 'low eta evt for', lbl, 'evtadjweight = ', evtadjweight
				if abs(dileptonMom(evt).Rapidity()) >= ysplit:  
#					print 'high eta evt for', lbl, 'evtadjweight = ', evtadjweight
					newhistadj_higheta.Fill(missPt(evt),evtadjweight) 
					
			eventadjCount+=1
#		print lbl, 'xsec = ', reader.xsec, 'eventadjCount = ', eventadjCount
		newhistadj_loweta.Scale(intluminosity*reader.xsec/eventadjCount,"width")
		newhistadj_higheta.Scale(intluminosity*reader.xsec/eventadjCount,"width")
 		
	##draw adjusted histograms
	#low eta
	for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist_loweta'].SetFillColor(i+2)
	for lbl in sampleDicts:
	    hist=sampleDicts[lbl]['adjhist_loweta']
#	    print 'bin entries low eta',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
	    hist.Draw("hist")
#	    print 'Histogram integral low eta for {0} = {1}'.format(lbl,hist.Integral())
	    ROOT.gPad.SetLogy();
	    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj_loweta.pdf')
	#high eta
	for i,lbl in enumerate(sampleDicts): sampleDicts[lbl]['adjhist_higheta'].SetFillColor(i+2)
	for lbl in sampleDicts:
	    hist=sampleDicts[lbl]['adjhist_higheta']
#	    print 'bin entries high eta',lbl,[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
	    hist.Draw("hist")
#	    print 'Histogram integral high eta for {0} = {1}'.format(lbl,hist.Integral())
	    ROOT.gPad.SetLogy();
	    ROOT.gPad.SaveAs(lbl+'fig4kfactoradj_higheta.pdf')
 	return

def fig4loweta_pTbinbybinPValues(sampleDicts, ysplit):
  print " low eta - pT bin by bin p values...."
###extract bin
  hist=sampleDicts['ZZ']['adjhist_loweta']
  zzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print zzbincontent
  hist=sampleDicts['WZ']['adjhist_loweta']
  wzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print wzbincontent
  hist=sampleDicts['DMsimpspin1']['adjhist_loweta']
  dmsimpbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print dmsimpbincontent
  bkgCentralValues=np.array(zzbincontent)+np.array(wzbincontent)
  sigCentralValues=np.array(dmsimpbincontent)
  ###generate 1000 pseudodata bin values for each bin, assuming bkg+sig rates and compute the log likelihood that each dataset comes from the bkg+sig distribution
  bin_pvalues = []
  for i in range(1,hist.GetNbinsX()):
	pseudodata=poissonPseudoExp(bkgCentralValues[i]+sigCentralValues[i],10000)
	pseudodataLogLikelihoods=[loglikelihood(data,bkgCentralValues[i]+sigCentralValues[i]) for data in pseudodata]
#	print "pseudodataLogLikelihoods: ", pseudodataLogLikelihoods
	###assume the actual data are measured to be just the central values of the bkg distribution, compute the likelihood that this data comes from bkg+sig  
 	bkgdiscreteValues=bkgCentralValues//1	
     	actualLogLikelihood=loglikelihood(bkgdiscreteValues[i],bkgCentralValues[i]+sigCentralValues[i])
#	print "actualLogLikelihood = ", actualLogLikelihood
     	###calculate p-value: the fraction of pseudodatasets which have a smaller log likelihood than the actual dataset
     	bin_pvalues.append(pValue(actualLogLikelihood,pseudodataLogLikelihoods))
  print "bin pvalues = ", bin_pvalues
  return bin_pvalues

def fig4higheta_pTbinbybinPValues(sampleDicts, ysplit):
  print " high eta - pT bin by bin p values...."
###extract bin
  hist=sampleDicts['ZZ']['adjhist_higheta']
  zzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print zzbincontent
  hist=sampleDicts['WZ']['adjhist_higheta']
  wzbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print wzbincontent
  hist=sampleDicts['DMsimpspin1']['adjhist_higheta']
  dmsimpbincontent=[hist.GetBinContent(i) for i in range(1,hist.GetNbinsX()+1)]
#  print dmsimpbincontent
  bkgCentralValues=np.array(zzbincontent)+np.array(wzbincontent)
  sigCentralValues=np.array(dmsimpbincontent)
  ###generate 1000 pseudodata bin values for each bin, assuming bkg+sig rates and compute the log likelihood that each dataset comes from the bkg+sig distribution
  bin_pvalues = []
  for i in range(1,hist.GetNbinsX()):
	pseudodata=poissonPseudoExp(bkgCentralValues[i]+sigCentralValues[i],10000)
	pseudodataLogLikelihoods=[loglikelihood(data,bkgCentralValues[i]+sigCentralValues[i]) for data in pseudodata]
#	print "pseudodataLogLikelihoods: ", pseudodataLogLikelihoods
	###assume the actual data are measured to be just the central values of the bkg distribution, compute the likelihood that this data comes from bkg+sig  
 	bkgdiscreteValues=bkgCentralValues//1
     	actualLogLikelihood=loglikelihood(bkgdiscreteValues[i],bkgCentralValues[i]+sigCentralValues[i])
#	print "actualLogLikelihood = ", actualLogLikelihood
     	###calculate p-value: the fraction of pseudodatasets which have a smaller log likelihood than the actual dataset
     	bin_pvalues.append(pValue(actualLogLikelihood,pseudodataLogLikelihoods))
  print "bin pvalues = ", bin_pvalues
#  print "bin 1 pvalue = ", bin_pvalues[1]
  
  return bin_pvalues

def fig4splitetapTbinbybinPvaluessearch(sampleDicts):
   print "Searching for optimal y split"
   lowest_pvalueloweta = 1.0
   lowest_pvalueloweta_ysplit = 0.0
   lowest_pvaluehigheta = 1.0
   lowest_pvaluehigheta_ysplit = 0.0
   bin1_loweta_pvalues = []
   bin2_loweta_pvalues = []
   bin3_loweta_pvalues = []
   bin4_loweta_pvalues = []
   bin5_loweta_pvalues = []
   bin6_loweta_pvalues = []
   bin7_loweta_pvalues = []
   bin8_loweta_pvalues = []
   bin9_loweta_pvalues = []
   bin1_higheta_pvalues = []
   bin2_higheta_pvalues = []
   bin3_higheta_pvalues = []
   bin4_higheta_pvalues = []
   bin5_higheta_pvalues = []
   bin6_higheta_pvalues = []
   bin7_higheta_pvalues = []
   bin8_higheta_pvalues = []
   bin9_higheta_pvalues = []
   ysplits_used = []

   
   for i in range(0,15):
#  for i in range(0,3):
	   ysplittest = 0.001 + i/4.
	   print "ysplit = ", ysplittest
	   fig4_Splitineta(sampleDicts, ysplittest)
	   pvalueloweta = fig4loweta_pTbinbybinPValues(sampleDicts, ysplittest)
	   pvaluehigheta = fig4higheta_pTbinbybinPValues(sampleDicts, ysplittest)
           bin1_loweta_pvalues.append(pvalueloweta[0])
           bin2_loweta_pvalues.append(pvalueloweta[1])
           bin3_loweta_pvalues.append(pvalueloweta[2])
           bin4_loweta_pvalues.append(pvalueloweta[3])
           bin5_loweta_pvalues.append(pvalueloweta[4])
           bin6_loweta_pvalues.append(pvalueloweta[5])
           bin7_loweta_pvalues.append(pvalueloweta[6])
           bin8_loweta_pvalues.append(pvalueloweta[7])
           bin9_loweta_pvalues.append(pvalueloweta[8])
#          print "bin1_loweta_pvalues = ", bin1_loweta_pvalues

           bin1_higheta_pvalues.append(pvaluehigheta[0])
           bin2_higheta_pvalues.append(pvaluehigheta[1])
           bin3_higheta_pvalues.append(pvaluehigheta[2])
           bin4_higheta_pvalues.append(pvaluehigheta[3])
           bin5_higheta_pvalues.append(pvaluehigheta[4])
           bin6_higheta_pvalues.append(pvaluehigheta[5])
           bin7_higheta_pvalues.append(pvaluehigheta[6])
           bin8_higheta_pvalues.append(pvaluehigheta[7])
           bin9_higheta_pvalues.append(pvaluehigheta[8])

           ysplits_used.append(ysplittest)
                        
           for j in range(0,len(pvalueloweta)-1):	
		if pvalueloweta[j] < lowest_pvalueloweta:
		   	lowest_pvalueloweta = pvalueloweta[j]
   			lowest_pvalueloweta_ysplit = ysplittest
   	   for j in range(0,len(pvaluehigheta)-1):
		if pvaluehigheta[j] < lowest_pvaluehigheta:	
		   	lowest_pvaluehigheta = pvaluehigheta[j]
   			lowest_pvaluehigheta_ysplit = ysplittest

   print "lowest p value for low eta events achieved for ysplit = ", lowest_pvalueloweta_ysplit, "and gives pvalue_loweta = ", lowest_pvalueloweta
   print "lowest p value for high eta events achieved for ysplit = ", lowest_pvaluehigheta_ysplit, "and gives pvalue_higheta = ", lowest_pvaluehigheta

   print "y splits used = ", ysplits_used
   
   print "bin1_loweta_pvalues = ", bin1_loweta_pvalues
   print "bin2_loweta_pvalues = ", bin2_loweta_pvalues
   print "bin3_loweta_pvalues = ", bin3_loweta_pvalues
   print "bin4_loweta_pvalues = ", bin4_loweta_pvalues
   print "bin5_loweta_pvalues = ", bin5_loweta_pvalues
   print "bin6_loweta_pvalues = ", bin6_loweta_pvalues
   print "bin7_loweta_pvalues = ", bin7_loweta_pvalues
   print "bin8_loweta_pvalues = ", bin8_loweta_pvalues
   print "bin9_loweta_pvalues = ", bin9_loweta_pvalues

   print "bin1_higheta_pvalues = ", bin1_higheta_pvalues
   print "bin2_higheta_pvalues = ", bin2_higheta_pvalues
   print "bin3_higheta_pvalues = ", bin3_higheta_pvalues
   print "bin4_higheta_pvalues = ", bin4_higheta_pvalues
   print "bin5_higheta_pvalues = ", bin5_higheta_pvalues
   print "bin6_higheta_pvalues = ", bin6_higheta_pvalues
   print "bin7_higheta_pvalues = ", bin7_higheta_pvalues
   print "bin8_higheta_pvalues = ", bin8_higheta_pvalues
   print "bin9_higheta_pvalues = ", bin9_higheta_pvalues

   plt.plot(ysplits_used, bin1_loweta_pvalues, color='g')
   plt.plot(ysplits_used, bin2_loweta_pvalues, color='orange')
   plt.plot(ysplits_used, bin3_loweta_pvalues, color='r')
   plt.plot(ysplits_used, bin4_loweta_pvalues, color='yellow')
   plt.plot(ysplits_used, bin5_loweta_pvalues, color='b')
   plt.plot(ysplits_used, bin6_loweta_pvalues, color='black')
   plt.plot(ysplits_used, bin7_loweta_pvalues, color='pink')
   plt.plot(ysplits_used, bin8_loweta_pvalues, color='cyan')
   plt.plot(ysplits_used, bin9_loweta_pvalues, color='purple')
   plt.xlabel('ysplit')
   plt.ylabel('pvalue')
   plt.title('Bin p values vs y split used')
   plt.legend()
   plt.show()

   
#   for i in range(5):
#	ysplittest = 0.1+i/0.90
#	print "ysplit = ", ysplittest
#	fig4_Splitineta(sampleDicts, ysplittest)
#	pvalueloweta = fig4loweta_pTbinbybinPValues(sampleDicts, ysplittest)
#	pvaluehigheta = fig4higheta_pTbinbybinPValues(sampleDicts, ysplittest)
#	if pvalueloweta < lowest_pvalueloweta:
#		lowest_pvalueloweta = pvalueloweta
#		lowest_pvalueloweta_ysplit = ysplittest
 #  print "lowest p value for low eta events achieved for ysplit = ", lowest_pvalueloweta_ysplit, "and gives pvalue_loweta = ", lowest_pvalueloweta
   return
	


if __name__=='__main__':
  plotPtY()
  sys.exit(0)
  sampleDicts=makeFig4()
#  fig4temptestPValue(sampleDicts)
  fig4PValue(sampleDicts)
  fig4pTbinbybinPValues(sampleDicts)
#  fig4_Splitineta(sampleDicts, 1.0)
#  fig4loweta_pTbinbybinPValues(sampleDicts, 1.0)
#  fig4higheta_pTbinbybinPValues(sampleDicts, 1.0)
  fig4splitetapTbinbybinPvaluessearch(sampleDicts)
#  doCutFlow()
#  plotPtY()
