launch ZZjet
shower=PYTHIA8
madspin=ON
madanalysis=ON
set nevents 10
set ptl 0.0
set ptj 7.0
set mll_sf 0.0
set jetalgo 1.0
set jetradius 1.0
set ickkw 3
set Qcut 30.0
set njmax 1
edit madspin --replace_line="decay z" decay z > l+ l-
edit madspin --after_line="decay z" decay z > nu nubar
edit madspin --replace_line="decay w+" decay w+ > l+ all
edit madspin --replace_line="decay w-" decay w- > l- all
#edit shower --replace_line="EXTRALIBS" EXTRALIBS    = stdhep Fmcfio dl  # Extra-libraries (not LHAPDF)
