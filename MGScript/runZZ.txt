launch ZZ
shower=PYTHIA8
madspin=ON
set nevents 100000
set ptl 0.0
set ptj 0.0
set mll_sf 0.0
set jetalgo -1
set jetradius 0.4
edit madspin --replace_line="decay z" decay z > l+ l-
edit madspin --after_line="decay z" decay z > nu nubar
edit madspin --replace_line="decay w+" decay w+ > l+ all
edit madspin --replace_line="decay w-" decay w- > l- all
#edit shower --replace_line="EXTRALIBS" EXTRALIBS    = stdhep Fmcfio dl  # Extra-libraries (not LHAPDF)
