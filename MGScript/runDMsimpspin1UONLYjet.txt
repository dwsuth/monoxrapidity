launch DMsimpspin1UONLYjet
shower=PYTHIA8
madspin=ON
madanalysis=ON
#set gVd31 0.001
set my1 500
set mxd 150
set nevents 10000
set ptl 0.0
set ptj 7.0
set mll_sf 0.0
set jetalgo 1.0
set jetradius 1.0
set ickkw 3
set Qcut 30.0
set njmax 1
edit madspin --replace_line="decay z" decay z > l+ l-
edit madspin --after_line="decay z" decay y1 > xd xd~
#edit shower --replace_line="EXTRALIBS" EXTRALIBS    = stdhep Fmcfio dl  # Extra-libraries (not LHAPDF)
