(* ::Package:: *)

(* ::Input:: *)
(*(*import nnpdf and define useful functions*)*)
(*Clear["Global`*"];*)
(*pdfpath = "mathematicapdf/math_package_v2/";*)
(*SetDirectory[pdfpath];*)
(*<< NNPDF`;*)
(*ResetDirectory[];*)
(*InitializePDFGrid["mathematicapdf/","NNPDF30_nnlo_as_0118"];*)
(*TwoBodyCOM[shat_,m1_,m2_]:=Module[{pstar,vec1,vec2,theta,phi,rotMat1,rotMat2},*)
(*pstar=Sqrt[(shat-(m1-m2)^2)(shat-(m1+m2)^2)/4/shat];*)
(*vec1={Sqrt[pstar^2+m1^2],0,0,pstar};*)
(*vec2={Sqrt[pstar^2+m2^2],0,0,-pstar};*)
(*theta=RandomReal[{0,Pi}];*)
(*phi=RandomReal[{0,2*Pi}];*)
(*rotMat1={*)
(*{1,0,0,0},*)
(*{0,Cos[theta],0,Sin[theta]},*)
(*{0,0,1,0},*)
(*{0,-Sin[theta],0,Cos[theta]}*)
(*};*)
(*rotMat2={*)
(*{1,0,0,0},*)
(*{0,Cos[phi],Sin[phi],0},*)
(*{0,-Sin[phi],Cos[phi],0},*)
(*{0,0,0,1}*)
(*};*)
(*{rotMat2.rotMat1.vec1,rotMat2.rotMat1.vec2}*)
(*]*)
(*pt[fourvec_]:=Sqrt[fourvec[[2]]^2+fourvec[[3]]^2];*)
(*ee[fourvec_]:=fourvec[[1]];*)
(*wye[fourvec_]:=Module[{pt},*)
(*pt=Sqrt[fourvec[[2]]^2+fourvec[[3]]^2];*)
(*ArcSinh[fourvec[[4]]/pt]*)
(*]*)
(*psqr[fourvec_]:={fourvec}.{{1,0,0,0},{0,-1,0,0},{0,0,-1,0},{0,0,0,-1}}.Transpose[{fourvec}][[1,1]]*)
(*partonLumi[x_]:=NIntegrate[xPDFcv[y,1000^2,1] xPDFcv[x/y,1000^2,1]/(x*y),{y,x,1}]*)
(*boostDist[s_,t_]:=Module[{x1,x2},*)
(*x1=Sqrt[s/(1-t^2)]+Sqrt[s t^2/(1-t^2)];*)
(*x2=Sqrt[s/(1-t^2)]-Sqrt[s t^2/(1-t^2)];*)
(*If[Abs[t]<1&&x1<1&&x2<1,xPDFcv[x1,1000^2,0]/x1 xPDFcv[x1,1000^2,0]/x2/(1-t^2),0]*)
(*]*)


(* ::Input:: *)
(*(*make distribution of parton luminosity*)*)
(*xs=Range[0.001,0.99,0.0002];*)
(*plvals=partonLumi[#]&/@xs;*)
(*plvalssanit=If[#>0,#,0]&/@plvals;*)
(*partonLumiED=EmpiricalDistribution[plvalssanit->xs]*)


(* ::Input:: *)
(*(*(*make distribution of PDF*)*)*)
(*xs=Range[0.01,0.99,0.0002];*)
(*pdfvals=xPDFcv[#,1000^2,1]/#&/@xs;*)
(*ed=EmpiricalDistribution[pdfvals->xs]*)


(* ::Input:: *)
(*(*two methods to generate two body final state four momenta, masses m1 and m2*)*)
(*TwoBodyLab[s_,m1_,m2_]:=Module[{x1,x2,ttred,shat,beta,gamma,boostMat,p3,p4},*)
(*x1=0;x2=0;*)
(*While[(m1+m2)^2<s&& x1 x2 s <=(m1+m2)^2,*)
(*x1=RandomVariate[TruncatedDistribution[{(m1+m2)^2/s,1.0},ed]];*)
(*x2=RandomVariate[TruncatedDistribution[{(m1+m2)^2/s,1.0},ed]];];*)
(*shat=x1 x2 s;*)
(*beta=(x1-x2)/(x1+x2);*)
(*(*ttred=TruncatedDistribution[{{(m1+m2)^2/s,1},{0,1}},tred];*)
(*{shat,beta}=RandomVariate[ttred];*)
(*shat=s*shat;*)*)
(*gamma=(1-beta^2)^(-1/2);*)
(*boostMat={*)
(*{gamma,0,0,gamma beta},*)
(*{0,1,0,0},*)
(*{0,0,1,0},*)
(*{gamma beta,0,0,gamma}*)
(*};*)
(*{p3,p4}=TwoBodyCOM[shat,m1,m2];*)
(*(*{p3,p4}=TwoBodyCOM[x1 x2 s,m1,m2];*)*)
(*{boostMat.p3,boostMat.p4}*)
(*]*)
(*TwoBodyLab2[s_,m1_,m2_]:=Module[{x1,x2,ttred,shat,beta,gamma,boostMat,p3,p4,u,shatovers,bdzero},*)
(*shatovers=RandomVariate[TruncatedDistribution[{(m1+m2)^2/s,1.0},partonLumiED]];*)
(*shat=shatovers s;*)
(*(*importance sample from boostdist*)*)
(*beta=0;*)
(*u=1.1;*)
(*bdzero=boostDist[shatovers,0];*)
(*While[u>boostDist[shatovers,beta]/bdzero,*)
(*beta=RandomReal[{-1,1}];*)
(*u=RandomReal[{0,1}];];*)
(*(*ttred=TruncatedDistribution[{{(m1+m2)^2/s,1},{0,1}},tred];*)
(*{shat,beta}=RandomVariate[ttred];*)
(*shat=s*shat;*)*)
(*gamma=(1-beta^2)^(-1/2);*)
(*boostMat={*)
(*{gamma,0,0,gamma beta},*)
(*{0,1,0,0},*)
(*{0,0,1,0},*)
(*{gamma beta,0,0,gamma}*)
(*};*)
(*{p3,p4}=TwoBodyCOM[shat,m1,m2];*)
(*(*{p3,p4}=TwoBodyCOM[x1 x2 s,m1,m2];*)*)
(*{boostMat.p3,boostMat.p4}*)
(*]*)


(* ::Input:: *)
(*vecs2000=Table[TwoBodyLab2[13000^2,0,2000][[1]],{ntrials,1,10000}];*)
(*vecs500=Table[TwoBodyLab2[13000^2,0,500][[1]],{ntrials,1,10000}];*)
(*vecs200=Table[TwoBodyLab2[13000^2,0,200][[1]],{ntrials,1,10000}];*)


(* ::Input:: *)
(*ListPlot[{Transpose[{Abs@*wye/@vecs500,pt/@vecs500}],Transpose[{Abs@*wye/@vecs2000,pt/@vecs2000}]}]*)


(* ::Input:: *)
(*binOpts={{0,6,0.2},{0,2000,40}};*)
(*xbins=(Range@@binOpts[[1]])[[;;-2]]*)
(*ybins=(Range@@binOpts[[2]])[[;;-2]]*)
(*plotRange={{0,6},{0,2000},{0,0.1}};*)
(*plotRange={Automatic,Automatic,{0,0.1}};*)
(*dh200=DensityHistogram[Transpose[{Abs@*wye/@vecs200,pt/@vecs200}],binOpts,"Probability",FrameLabel->{"|y|","\!\(\*SubscriptBox[\(p\), \(T\)]\)/GeV"},LabelStyle->16,PlotRange->plotRange,ChartLegends->Automatic]*)
(*dh500=DensityHistogram[Transpose[{Abs@*wye/@vecs500,pt/@vecs500}],binOpts,"Probability",FrameLabel->{"|y|","\!\(\*SubscriptBox[\(p\), \(T\)]\)/GeV"},LabelStyle->16,PlotRange->plotRange,ChartLegends->Automatic]*)
(*dh2000=DensityHistogram[Transpose[{Abs@*wye/@vecs2000,pt/@vecs2000}],binOpts,"Probability",FrameLabel->{"|y|","\!\(\*SubscriptBox[\(p\), \(T\)]\)/GeV"},LabelStyle->16,PlotRange->plotRange,ChartLegends->Automatic]*)
(*Export["figures/PtYDistM200.pdf",dh200]*)
(*Export["figures/PtYDistM500.pdf",dh500]*)
(*Export["figures/PtYDistM2000.pdf",dh2000]*)


(* ::Input:: *)
(*pl=Histogram[Map[ee,{vecs200,vecs500,vecs2000},{2}],Automatic,"Probability",ChartStyle->Opacity[0.3],LabelStyle->16,ChartLegends->{"200","500","2000"},AxesLabel->{"E",None}]*)
(*Export["figures/EDist.pdf",pl]*)
(*pl=Histogram[Map[pt,{vecs200,vecs500,vecs2000},{2}],Automatic,"Probability",ChartStyle->Opacity[0.3],LabelStyle->16,ChartLegends->{"200","500","2000"},AxesLabel->{"\!\(\*SubscriptBox[\(p\), \(T\)]\)",None}]*)
(*Export["figures/ptDist.pdf",pl]*)
(*pl=Histogram[Map[Abs@*wye,{vecs200,vecs500,vecs2000},{2}],Automatic,"Probability",ChartStyle->Opacity[0.2],LabelStyle->16,ChartLegends->{"200","500","2000"},AxesLabel->{"|y|",None}]*)
(*Export["figures/yDist.pdf",pl]*)


(* ::Input:: *)
(*Histogram[WeightedData[pt/@vecs200,(1.5)&]]*)


(* ::Input:: *)
(*{g,{binCounts}}=Reap[Histogram[RandomVariate[NormalDistribution[0,1],100],{-2,2,0.25},Function[{bins,counts},Sow[counts]]]]*)


(* ::Input:: *)
(*{crap,dh200counts}=Reap[DensityHistogram[Transpose[{Abs@*wye/@vecs200,pt/@vecs200}],binOpts,Function[{bins,binsz,counts},Sow[counts]]]];*)
(*{crap,dh500counts}=Reap[DensityHistogram[Transpose[{Abs@*wye/@vecs500,pt/@vecs500}],binOpts,Function[{bins,binsz,counts},Sow[counts]]]];*)
(*{crap,dh2000counts}=Reap[DensityHistogram[Transpose[{Abs@*wye/@vecs2000,pt/@vecs2000}],binOpts,Function[{bins,binsz,counts},Sow[counts]]]];*)


(* ::Input:: *)
(*xybins=Outer[{#1,#2}&,xbins,ybins];*)
(*histdata=Join[xybins,Map[{#}&,(dh2000counts-dh500counts)[[1,1]],{2}],3];*)
(*pl=ListDensityPlot[Flatten[histdata,1],DataRange->Automatic,InterpolationOrder->0,PlotRange->{Automatic,Automatic,{-100,30}},PlotLegends->BarLegend[{Automatic,{-100,30}}],FrameLabel->{"|y|","\!\(\*SubscriptBox[\(p\), \(T\)]\)/GeV"},LabelStyle->16]*)
(*Export["figures/diffptYM500M2000.pdf",pl]*)


(* ::Input:: *)
(*MatrixPlot[(dh2000counts-dh500counts)[[1,1]]]*)


(* ::Input:: *)
(*(*ignore the stuff below*)*)


(* ::Input:: *)
(*Histogram[RandomVariate[NormalDistribution[0,1],100],{-2,2,0.25},Function[{bins,counts},Sow[counts]]]*)


(* ::Input:: *)
(*Histogram[ys]*)
(*Histogram[pts]*)
(*Histogram[es]*)


(* ::Input:: *)
(*vecs=Table[TwoBodyCOM[13000^2,0,1000][[1]],{ntrials,1,10000}];*)
(*pts=pt/@vecs;*)
(*ys=wye/@vecs;*)
(*es=ee/@vecs;*)


(* ::Input:: *)
(*pl1=Histogram[ys,Automatic,"PDF"]*)
(*pl2=Plot[1/2/Cosh[y]^2,{y,-5,5}]*)
(*Show[pl1,pl2]*)


(* ::Input:: *)
(**)


(* ::Input:: *)
(*boostDist[0.01,0.999]*)


(* ::Input:: *)
(*Plot[Evaluate[boostDist[#,t]/boostDist[#,0]&/@{0.01,0.1,0.3,0.5}],{t,-0.9,0.9}]*)


(* ::Input:: *)
(*DensityPlot[xPDFcv[x1,1000^2,0]/x1 xPDFcv[x2,1000^2,0]/x2,{x1,0,1},{x2,0,1}]*)


(* ::Input:: *)
(*max=1*^4;*)
(*min=1*^-2;*)
(*sf=Log[#/min]/Log[max/min]&;*)
(*isf=InverseFunction[sf];*)
(*DensityPlot[xPDFcv[x1,1000^2,0]/x1 xPDFcv[x2,1000^2,0]/x2,{x1,0,0.8},{x2,0,0.8},PlotRange->All,PlotPoints->100,ScalingFunctions->{sf,isf},ColorFunction->"DeepSeaColors",PlotRange->{min,max},ColorFunctionScaling->False,PlotLegends->BarLegend[{"DeepSeaColors",{min,max}},ScalingFunctions->{sf,isf}]]*)


(* ::Input:: *)
(*xPDFcv[0.01,1000^2,0]*)


(* ::Input:: *)
(*pl=Plot[{1/Cosh[y],2/Cosh[y],1/Cosh[y-1]},{y,-3,3},AxesLabel->{"y","\!\(\*SubscriptBox[\(p\), \(T\)]\)"},PlotLegends->{"\!\(\*SubscriptBox[\(p\), \(T\)]\) cosh(y) = const.","Change \!\(\*SubscriptBox[\(x\), \(1\)]\)\!\(\*SubscriptBox[\(x\), \(2\)]\)","Change \!\(\*SubscriptBox[\(x\), \(\(1\)\(\\\ \)\)]\)-\!\(\*SubscriptBox[\(x\), \(2\)]\)"}]*)
(*Export["figures/partonLevelCurves.pdf",pl]*)



